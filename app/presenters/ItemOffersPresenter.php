<?php

namespace App\Presenters;

use Nette,
	App\Model\Facades\ItemOfferFacade,
	App\Forms\ItemOfferFormFactory,
	Traits\ItemTooltipHelper,
	App\Model\Entities\ItemOffer,
	App\Model\Entities\ViewPermissions;


class ItemOffersPresenter extends BasePresenter
{

	use ItemTooltipHelper;
	
	/**
	 * @var ItemOfferFacade
	 * @inject
	 */
	public $itemOfferFacade;
	
	/**
	 * @var ItemOfferFormFactory
	 * @inject
	 */
	public $formFactory;
	
	/** @var ItemOffer */
	private $itemOffer;
	
	
	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::ITEM_OFFERS)) {
			$this->redirect("Homepage:default");
		}
	}
	

	public function renderDefault()
	{
		$this->template->users = $this->itemOfferFacade->getUsersWithOffersCount();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderUser($id = NULL)
	{
		$this->template->searchedUser = $this->userFacade->getUser($id);
		
		$this->template->addFilter("itemOfferTooltip", $this->helperGenerateItemOfferTooltip);
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionEdit($id = NULL)
	{
		$id = (int) $id;
		$this->itemOffer = $offer = $this->userEntity->getItemOffer($id);
		if ($this->itemOffer !== NULL) {
			$prefix = $offer->prefix !== NULL ? $offer->prefix->name : NULL;
			$suffix = $offer->suffix !== NULL ? $offer->suffix->name : NULL;
			
			$this["editOfferForm"]->setDefaults(array(
				"offerId" => $id,
				"prefix" => $prefix,
				"itemName" => $offer->item->name,
				"suffix" => $suffix,
				"minDamage" => $offer->itemMinDamage,
				"maxDamage" => $offer->itemMaxDamage,
				"armour" => $offer->itemArmour,
				"improvementType" => $offer->itemImprovementType,
				"improvementValue" => $offer->itemImprovementValue,
				"quality" => $offer->itemQuality,
				"price" => $offer->price
			));
		}
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderEdit($id = NULL)
	{
		$this->template->offer = $this->itemOffer;
	}
	
	
	
	/**
	 * @return Form
	 */
	public function createComponentOfferItemForm()
	{
		return $this->formFactory->createOfferItem();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentEditOfferForm()
	{
		return $this->formFactory->createEditOffer();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleDeleteOffer($id = NULL)
	{
		$id = (int) $id;
		if ($id > 0) try {
			$this->itemOfferFacade->deleteOffer($this->userEntity, $id);
			$this->flashMessage("Nabídka byla odstraněna", "success");
		} catch (Nette\InvalidArgumentException $e) {
			$this->flashMessage($e->getMessage(), "error");
		}
		
		$this->redirect("user", array("id" => $this->user->id));
	}

}
