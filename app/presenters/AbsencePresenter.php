<?php

namespace App\Presenters;

use Nette,
	App\Model\Facades\AbsenceFacade,
	App\Forms\AbsenceFormFactory,
	App\Model\Entities\ViewPermissions;


class AbsencePresenter extends BasePresenter
{

	/**
	 * @var AbsenceFacade
	 * @inject
	 */
	public $absenceFacade;
	
	/**
	 * @var AbsenceFormFactory
	 * @inject
	 */
	public $formFactory;
	
	
	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::ABSENCES)) {
			$this->redirect("Homepage:default");
		}
	}
	

	public function renderDefault()
	{
		$this->template->absences = $this->userEntity->getAbsences();
		
		if ($this->userEntity->isAdmin()) {
			$this->template->usersAbsences = $this->absenceFacade->getUsersAbsences(array($this->userEntity));
		}
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentAddForm()
	{
		return $this->formFactory->createAdd();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleDeleteAbsence($id = NULL)
	{
		if ($id > 0) try {
			$this->absenceFacade->deleteAbsence($this->userEntity, $id);
			$this->flashMessage("Absence byla smazána", "success");
		} catch (Nette\InvalidArgumentException $e) {
			$this->flashMessage($e->getMessage(), "error");
		}
		
		$this->redirect("default");
	}

}
