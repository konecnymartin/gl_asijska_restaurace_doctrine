<?php

namespace App\Presenters;

use Nette,
	App\Forms\NotificationFormFactory,
	App\Model\Entities\ViewPermissions;


class NotificationPresenter extends BasePresenter
{

	/**
	 * @var NotificationFormFactory
	 * @inject
	 */
	public $formFactory;
	
	
	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::NOTIFICATIONS)) {
			$this->redirect("Homepage:default");
		}
	}
	

	public function renderDefault()
	{
		$this->template->allNotifications = $this->notificationFacade->getNotifications();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentAddForm()
	{
		return $this->formFactory->createAdd();
	}

}
