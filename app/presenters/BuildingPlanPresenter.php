<?php

namespace App\Presenters;

use Nette,
	App\Forms\BuildingPlanFormFactory,
	App\Model\Entities\ViewPermissions;


class BuildingPlanPresenter extends BasePresenter
{
	
	/**
	 * @var BuildingPlanFormFactory
	 * @inject
	 */
	public $formFactory;
	
	/** @var BuildingPlan */
	private $actualPlan = NULL;
	
	
	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::BUILDING_PLANS)) {
			$this->redirect("Homepage:default");
		}
	}
	

	public function renderDefault()
	{
		$this->template->plans = $this->buildingPlanFacade->getPlans();
		
		$this->template->addFilter("buildingToString", $this->getBuildingName);
	}
	
	
	public function actionCreate()
	{
		$form = $this["createPlanForm"];
		$form["building"]->setItems($this->getBuildingNames());
		$users = $this->userFacade->getVisibleUsersList();
		
		foreach ($users as $user) {
			$form["usersInformations"][$user->id]->setDefaults(array(
				"donationRequired" => TRUE
			));
		}
		
		$userNames = array();
		foreach ($users as $user) {
			$userNames[$user->id] = $user->name;
		}
		
		$this->template->userNames = $userNames;
	}
	
	
	public function renderCreate()
	{
		$this->template->users = $this->userFacade->getVisibleUsersList();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionDetail($id = NULL)
	{
		$id = (int) $id;
		$this->actualPlan = $this->buildingPlanFacade->getPlan($id);
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderDetail($id = NULL)
	{
		if ($this->actualPlan !== NULL) {
			$userInformations = $this->buildingPlanFacade->getInformationsForUser($this->userEntity, $this->actualPlan);
			$savedGoldSum = $this->buildingPlanFacade->getSavedGoldSum($this->actualPlan);

			if ($this->actualPlan->expired()) {
				$this["setSavedGoldForm"]["sum"]->setDisabled(TRUE);
				$this["setSavedGoldForm"]["setSavedGold"]->setDisabled(TRUE);
			}
			
			$this["setSavedGoldForm"]->setDefaults(array(
				"planId" => $id,
				"sum" => $userInformations->savedGold
			));
			
			$donationAmount = 0;
			$donation = $this->buildingPlanFacade->getUsersDonationForPlan($this->userEntity, $this->actualPlan);
			if ($donation !== NULL) {
				$donationAmount = $donation->amount;
			}
			
			$this["setDonationForm"]->setDefaults(array(
				"planId" => $id,
				"amount" => $donationAmount
			));

			$this->template->usersInformations = $this->buildingPlanFacade->getUsersInformations($this->actualPlan);
			$this->template->userInformations = $userInformations;
			$this->template->usersDonation = $this->actualPlan->calculateUsersDonation($this->userEntity);
			$this->template->savedGoldSum = $savedGoldSum;
			$this->template->isPriceSaved = $savedGoldSum >= $this->actualPlan->building->getPrice();
			
			// it's needed to set it here, because when signals are received, it's still on this page
			$this->buildingPlanFacade->addVisit($this->userEntity, $this->actualPlan);
		}
		
		$this->template->plan = $this->actualPlan;
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionEdit($id = NULL)
	{
		$id = (int) $id;
		$this->actualPlan = $plan = $this->buildingPlanFacade->getPlan($id);
		if ($plan !== NULL) {
			$this["editPlanForm"]["building"]->setItems($this->getBuildingNames());
			$this["editPlanForm"]->setDefaults(array(
				"planId" => $id,
				"building" => $plan->building->key,
				"discount" => $plan->building->percentualDiscount,
				"level" => $plan->building->level,
				"dateFrom" => $plan->from->format("d.m.Y H:i:s"),
				"dateTo" => $plan->to->format("d.m.Y H:i:s"),
				"comment" => $plan->comment
			));
		}
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderEdit($id = NULL)
	{
		$this->template->plan = $this->actualPlan;
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionAddDonationInfo($id = NULL)
	{
		$id = (int) $id;
		$this->actualPlan = $plan = $this->buildingPlanFacade->getPlan($id);
		if ($plan !== NULL) {
			$users = $this->userFacade->getGuildUsersList();
			$options = array();
			foreach ($users as $user) {
				$options[$user->id] = $user->name;
			}
			
			$this["addDonationInfoForm"]["user"]->setItems($options);
			$this["addDonationInfoForm"]->setDefaults(array("planId" => $id));
		}
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderAddDonationInfo($id = NULL)
	{
		$this->template->plan = $this->actualPlan;
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionVisits($id = NULL)
	{
		$this->actualPlan = $plan = $this->buildingPlanFacade->getPlan($id);
		
		if ($this->actualPlan === NULL || !$this->userEntity->isRoot()) {
			$this->redirect("default");
		}
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderVisits($id = NULL)
	{
		$this->template->visitsAll = $this->buildingPlanFacade->getVisits(array(
			"plan" => $this->actualPlan,
			"orderByDate" => "DESC"
		));
		
		$visitsDistinct = $this->buildingPlanFacade->getVisits(array(
			"plan" => $this->actualPlan,
			"distinctUsers" => TRUE
		));
	
		$users = $this->userFacade->getGuildUsersList();
		$noUsersVisits = array();
		
		foreach ($users as $user) {
			$found = FALSE;
			
			foreach ($visitsDistinct as $userVisit) {
				if ($userVisit->user === $user) {
					$found = TRUE;
				}
			}
			
			if (!$found) {
				$noUsersVisit[] = $user->name;
			}
		}		
		
		$this->template->noUsersVisit = $noUsersVisit;
	}
	

	/**
	 * @return Form
	 */
	public function createComponentCreatePlanForm()
	{	
		return $this->formFactory->createCreatePlan();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentSetSavedGoldForm()
	{
		return $this->formFactory->createSetSavedGold();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentEditPlanForm()
	{	
		return $this->formFactory->createEditPlan();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentAddDonationInfoForm()
	{	
		return $this->formFactory->createAddDonationInfo();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentSetDonationForm()
	{	
		return $this->formFactory->createSetDonation();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleDeletePlan($id = NULL)
	{
		$id = (int) $id;
		if ($id > 0) try {
			$this->buildingPlanFacade->deletePlan($this->userEntity, $id);
		} catch (Nette\InvalidArgumentException $e) {
			$this->flashMessage($e->getMessage(), "error");
		}
		
		$this->redirect("default");
	}

}
