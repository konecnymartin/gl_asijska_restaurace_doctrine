<?php

namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form,
	App\Forms\AdminFormFactory,
	App\Model\Entities\User as UserEntity,
	App\Model\Entities\ViewPermissions;


class AdminPresenter extends BasePresenter
{

	/**
	 * @var AdminFormFactory
	 * @inject
	 */
	public $formFactory;

	/** @var UserEntity */
	private $searchedUser;
	

	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->isAdmin()) {
			$this->redirect("Homepage:default");
		}
	}
	
	
	public function actionAddUser()
	{
		$this["addUserForm"]->setDefaults(array(
			"role" => UserEntity::ROLE_USER
		));
	}

	
	public function renderUsersList()
	{
		$this->template->users = $this->userFacade->getUsersList();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionEditUser($id = NULL)
	{
		$this->searchedUser = $user = $this->userFacade->getUser($id);
		if ($user !== NULL && !$user->isRoot()) {
			$vp = $user->viewPermissions;

			$this["editUserForm"]->setDefaults(array(
				"userId" => $id,
				"name" => $user->name,
				"gladiatusId" => $user->gladiatusId,
				"role" => $user->role,
				"hidden" => $user->hidden,
				"inGuild" => $user->inGuild,
				"description" => $user->description->content
			));
			
		
			$permissionsValues = ViewPermissions::getKeys();
			foreach ($permissionsValues as $key => $val) {
				if (!$vp->$key) {
					unset($permissionsValues->$key);
				}
			}
			
			$this["editUserForm"]->setDefaults(array("viewPermissions" => array_keys((array) $permissionsValues)));
		}
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderEditUser($id = NULL)
	{
		$this->template->searchedUser = $this->searchedUser;
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderUserInfo($id = NULL)
	{
		$this->template->searchedUser = $this->userFacade->getUser($id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentAddUserForm()
	{
		return $this->formFactory->createAddUser();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentEditUserForm()
	{
		return $this->formFactory->createEditUser();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleDeleteUser($id = NULL)
	{
		$id = (int) $id;
		if ($id > 0) try {
			$this->userFacade->deleteUser($id);
			$this->flashMessage("Uživatel byl odstraněn", "success");
		} catch (Nette\InvalidArgumentException $e) {
			$this->flashMessage($e->getMessage());
		}
		
		$this->redirect("usersList");
	}
	
}
