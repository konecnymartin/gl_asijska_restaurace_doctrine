<?php

namespace App\Presenters;

use Nette,
	App\Model\Entities\ViewPermissions;


class MembersPresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::ABOUT_MEMBERS)) {
			$this->redirect("Homepage:default");
		}
	}
	

	public function renderAbout()
	{
		$this->template->users = $this->userFacade->getUsersList(array("withDescription"));
	}

}
