<?php

namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form,
	App\Forms\HomepageFormFactory,
	App\Model\Facades\ItemOfferFacade,
	Traits\ItemTooltipHelper,
	App\Model\Entities\ViewPermissions;


class HomepagePresenter extends BasePresenter
{

	use ItemTooltipHelper;

	/**
	 * @var ItemOfferFacade
	 * @inject
	 */
	public $itemOfferFacade;

	/**
	 * @var HomepageFormFactory
	 * @inject
	 */
	public $formFactory;

	public function actionDefault()
	{	
		if (!$this->user->isLoggedIn()) {
			$this->setLayout("unlogged");
			$this->setView("unlogged");
		}
	}

	
	public function renderDefault()
	{
		if ($this->userEntity->canView(ViewPermissions::VOTINGS)) {
			$this->template->notVotedThemes = $this->votingFacade->getUsersNotVotedThemes($this->userEntity);
		}
		
		$this->template->latestItemOffers = $this->itemOfferFacade->getLatestOffers(10);
		
		$this->template->addFilter("itemOfferTooltip", $this->helperGenerateItemOfferTooltip);
	}
	
	
	public function renderUnlogged()
	{
		$this->template->itemOffers = $this->itemOfferFacade->getOffers();
		
		$this->template->addFilter("itemOfferTooltip", $this->helperGenerateItemOfferTooltip);
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentLoginForm()
	{
		return $this->formFactory->createLogin();
	}

}
