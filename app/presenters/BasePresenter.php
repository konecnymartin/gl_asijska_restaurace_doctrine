<?php

namespace App\Presenters;

use Nette,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	App\Model\Facades\BuildingPlanFacade,
	App\Model\Facades\VotingFacade,
	App\Model\Facades\NotificationFacade,
	App\Model\Entities\Building,
	App\Model\Entities\ViewPermissions,
	App\Model\DateHelper;


abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/**
	 * @var UserFacade
	 * @inject
	 */
	public $userFacade;
	
	/**
	 * @var BuildingPlanFacade
	 * @inject
	 */
	public $buildingPlanFacade;
	
	/**
	 * @var VotingFacade
	 * @inject
	 */
	public $votingFacade;
	
	/**
	 * @var NotificationFacade
	 * @inject
	 */
	public $notificationFacade;

	/** @var UserEntity */
	protected $userEntity;
	
	
	public function startup()
	{
		parent::startup();
		
		if ($this->user->isLoggedIn()) {
			$this->userEntity = $this->userFacade->getUser($this->user->id);
		} else {
			if ($this->getName() !== "Homepage" || $this->getAction() !== "default") {
				$this->redirect("Homepage:default");
			}
		
			// to be able to use conditions  if ($userEntity->isAdmin()) when the user is not logged in
			$entity = new UserEntity();
			$entity->role = UserEntity::ROLE_USER;
			
			$this->userEntity = $entity;
		}
	}
	
	
	public function beforeRender()
	{
		$actualPlans = array();
		$notifications = array();
		if ($this->user->isLoggedIn()) {
			if ($this->userEntity->canView(ViewPermissions::BUILDING_PLANS)) {
				$actualPlans = $this->buildingPlanFacade->getActualPlans(3);
			}
			if ($this->userEntity->canView(ViewPermissions::NOTIFICATIONS)) {
				$notifications = $this->notificationFacade->getUsersNotifications($this->userEntity);
			}
		} 
		
		$this->template->userEntity = $this->userEntity;
		$this->template->actualBuildingPlans = $actualPlans;
		$this->template->notifications = $notifications;
		
		if (!empty($actualPlans)) {
			$this->template->addFilter("buildingToString", $this->getBuildingName);
			$this->template->addFilter("dayNameOfDate", $this->getDayNameOfDate);
		}
		
		$this->template->viewPermissionsKeys = ViewPermissions::getKeys();
	}
	
	
	public function handleLogout()
	{
		$this->user->logout(TRUE);
		$this->redirect("Homepage:default");
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleDeleteNotification($id = NULL)
	{
		if ($id > 0) try {
			$this->notificationFacade->deleteNotification($this->userEntity, $id);
			$this->flashMessage("Oznámení bylo smazáno", "success");
		} catch (Nette\InvalidArgumentException $e) {
			$this->flashMessage($e->getMessage(), "error");
		}
		
		$this->redirect("default");
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleHideNotification($id = NULL)
	{
		if ($id > 0) try {
			$this->notificationFacade->hideNotification($this->userEntity, $id);
		} catch (Nette\InvalidArgumentException $e) {
		}
		
		if (!$this->isAjax()) {
			$this->redirect("default");
		}
	}
	
	
	/**
	 * @return array
	 */
	public function getBuildingNames()
	{
		return array(
			Building::FORUM => "Forum Gladiatorius",
			Building::BANK => "Banka",
			Building::TRAINING_GROUND => "Cvičiště",
			Building::MARKET => "Tržiště",
			Building::LIBRARY => "Knihovna",
			Building::BATHHOUSE => "Lázně",
			Building::NEGOTIUM_X => "Negotium X",
			Building::STORAGE => "Skladiště",
			Building::WAR_MASTER_HALL => "Síň mistra válek",
			Building::TEMPLUM => "Templum",
			Building::VILLA_MEDICI => "Villa Medici"
		);
	}
	
	
	/**
	 * @param string
	 * @return string
	 */
	public function getBuildingName($key)
	{
		$names = $this->getBuildingNames();
		
		return $names[$key];
	}
	
	/**
	 * @param \DateTime
	 * @return string
	 */
	public function getDayNameOfDate(\DateTime $date)
	{
		return DateHelper::getDayNameOfDate($date);
	}

}
