<?php

namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form,
	App\Forms\ContactFormFactory,
	App\Model\Entities\ViewPermissions;


class ContactPresenter extends BasePresenter
{
	
	/**
	 * @var ContactFormFactory
	 * @inject
	 */
	public $formFactory;
	

	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::CONTACT)) {
			$this->redirect("Homepage:default");
		}
	}
	
	
	public function actionDefault()
	{
		$users = $this->userFacade->getVisibleUsersList();
		$items = array();
		foreach ($users as $user) {
			$items[$user->id] = $user->name;
		}
		$this["contactForm"]["user"]->setItems($items);
	}
	
	
	/**
	 * @return Nette\Application\UI\Form
	 */ 
	public function createComponentContactForm()
	{
		return $this->formFactory->createContact();
	}

}
