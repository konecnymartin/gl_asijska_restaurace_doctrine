<?php

namespace App\Presenters;

use Nette,
	Nette\Application\UI\Form,
	App\Forms\VotingFormFactory,
	App\Model\Entities\VotingTheme,
	App\Model\Entities\ViewPermissions;


class VotingPresenter extends BasePresenter
{

	/**
	 * @var VotingFormFactory
	 * @inject
	 */
	public $formFactory;
	
	/** @var VotingTheme */
	private $theme;
	
	
	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::VOTINGS)) {
			$this->redirect("Homepage:default");
		}
	}
	

	public function renderDefault()
	{
		$this->template->themes = $this->votingFacade->getThemes();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function actionDetail($id)
	{
		$this->theme = $this->votingFacade->getTheme($id);
		if ($this->theme !== NULL) {
			$this["addCommentForm"]->setDefaults(array("themeId" => $id));
			$voteForm = $this["voteForm"];
			$voteForm->setDefaults(array("themeId" => $id));
			
			$items = array();
			foreach ($this->theme->answers as $answer) {
				$items[$answer->id] = $answer->content;
			}
			$voteForm["answer"]->setItems($items);
			
			if ($this->theme->expired() || $this->theme->userVoted($this->userEntity)) {
				$voteForm["answer"]->setDisabled(TRUE);
				$voteForm["vote"]->setDisabled(TRUE);
			}
		}
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function renderDetail($id)
	{
		if ($this->theme !== NULL) {
			$this->template->theme = $this->theme;
			$this->template->alreadyVoted = $this->theme->userVoted($this->userEntity);
			$this->template->usersVotes = $this->votingFacade->getUsersVotes($this->theme);
		}
	}

	
	/**
	 * @return Form
	 */
	public function createComponentCreateVotingForm()
	{
		return $this->formFactory->createCreateVoting();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentVoteForm()
	{
		return $this->formFactory->createVote();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentAddCommentForm()
	{
		return $this->formFactory->createAddComment();
	}
	
	
	/**
	 * @param int|NULL
	 */
	public function handleDeleteTheme($id = NULL)
	{
		$id = (int) $id;
		if ($id > 0) try {
			$this->votingFacade->deleteTheme($this->userEntity, $id);
			$this->flashMessage("Hlasování bylo odstraněno", "success");
		} catch (Nette\InvalidArgumentException $e) {
			$this->flashMessage($e->getMessage(), "error");
		}
		
		$this->redirect("default");
	}
	
}
