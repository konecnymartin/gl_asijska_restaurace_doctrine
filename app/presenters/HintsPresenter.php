<?php

namespace App\Presenters;

use Nette,
	App\Model\Entities\ViewPermissions;


class HintsPresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::HINTS)) {
			$this->redirect("Homepage:default");
		}
	}

}
