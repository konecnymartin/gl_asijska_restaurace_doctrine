<?php

namespace App\Presenters;

use Nette,
	App\Model\Entities\ViewPermissions;


class RulesPresenter extends BasePresenter
{

	public function startup()
	{
		parent::startup();
		
		if (!$this->userEntity->canView(ViewPermissions::RULES)) {
			$this->redirect("Homepage:default");
		}
	}

}
