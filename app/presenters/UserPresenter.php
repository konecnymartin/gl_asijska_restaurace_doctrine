<?php

namespace App\Presenters;

use Nette,
	App\Forms\UserFormFactory;


class UserPresenter extends BasePresenter
{

	/**
	 * @var UserFormFactory
	 * @inject
	 */
	public $formFactory;
	

	public function actionSettings()
	{
		$this["changeDescriptionForm"]->setDefaults(array("content" => $this->userEntity->description->content));
		$this["changeEmailForm"]->setDefaults(array("email" => $this->userEntity->email));
		
		$s = $this->userEntity->settings;
		$this["generalSettingsForm"]->setDefaults(array(
			"sendEmailWhenBuildingPlanCreated" => $s->sendEmailWhenBuildingPlanCreated,
			"levelPowerupActive" => $s->levelPowerupActive
		));
	}


	/**
	 * @return Form
	 */
	public function createComponentChangePasswordForm()
	{
		return $this->formFactory->createChangePassword();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentChangeEmailForm()
	{
		return $this->formFactory->createChangeEmail();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentChangeDescriptionForm()
	{
		return $this->formFactory->createChangeDescription();
	}
	
	
	/**
	 * @return Form
	 */
	public function createComponentGeneralSettingsForm()
	{
		return $this->formFactory->createGeneralSettings();
	}

}
