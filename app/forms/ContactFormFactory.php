<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	Nette\Mail\SendmailMailer,
	Nette\Mail\Message as MailMessage;


class ContactFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 */
	public function __construct(User $user, UserFacade $userFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createContact()
	{
		$form = new Form();
		
		$form->addSelect("user", "Uživatel")
			->setRequired();

		$form->addTextarea("content", "Zpráva:")
			->setRequired("Nevyplnil jsi zprávu")
			->setAttribute("rows", 8)
			->setAttribute("cols", 60);
		
		$form->addSubmit("sendMessage", "Odeslat zprávu");
		$form->onSuccess[] = $this->contactSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function contactSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		$from = $p->context->getParameters()["email"]["from"];
		
		try {
			$user = $this->userFacade->getUser($values->user, TRUE);
			if (empty($user->email)) {
				throw new Nette\InvalidArgumentException("Tento uživatel nemá nastavený e-mail");
			}
			
			$message = new MailMessage();
			$message
				->setFrom("Asijská restaurace <{$from}>")
				->addTo($user->email)
				->setSubject("Asijská restaurace ({$this->userEntity->name})")
				->setBody($values->content);
			
			$mailer = new SendmailMailer();
			$mailer->send($message);
			
			$p->flashMessage("Zpráva byla odeslána", "success");
			$p->redirect("this");
		} catch(Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}

}