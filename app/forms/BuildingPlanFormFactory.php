<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	App\Model\Facades\BuildingPlanFacade,
	Nette\Mail\SendmailMailer,
	Nette\Mail\Message as MailMessage,
	Nette\Utils\Validators,
	Doctrine\DBAL\Exception\UniqueConstraintViolationException,
	Nette\Forms\Container;


class BuildingPlanFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var BuildingPlan */
	private $buildingPlanFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 * @param BuildingPlanFacade
	 */
	public function __construct(User $user, UserFacade $userFacade, BuildingPlanFacade $buildingPlanFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		$this->buildingPlanFacade = $buildingPlanFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createCreatePlan()
	{
		$users = $this->userFacade->getVisibleUsersList();
		
		$form = new Form;
		$form->addSelect("building", "Budova")
			->setRequired();
			
		$form->addText("level", "Úroveň")
			->setRequired("Nezadal jsi úroveň")
			->addRule(Form::INTEGER, "Úroveň není číslo");
		
		$form->addText("discount", "Sleva (%)")
			->setRequired("Nezadal jsi slevu")
			->addRule(Form::INTEGER, "Sleva není číslo")
			->setDefaultValue(0);
			
		$form->addText("dateFrom", "Od")
			->setRequired("Nebylo zadáno datum od");
		
		$form->addText("dateTo", "Do")
			->setRequired("Nebylo zadáno datum do");
		
		$form->addCheckbox("optional", "Dobrovolná")
			->setDefaultValue(FALSE);
			
		$form->addTextarea("comment", "Komentář")
			->setAttribute("rows", 6)
			->setAttribute("cols", 50);

		$form->addDynamic("usersInformations", function(Container $container) {
			$container->addCheckbox("donationRequired");
		});
		
		$form->addSubmit("create", "Vytvořit plán");
		$form->onSuccess[] = $this->createPlanSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function createPlanSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		$levelsFrom = $form->getHttpData(Form::DATA_TEXT, "levelsFrom[]");
		$levelsTo = $form->getHttpData(Form::DATA_TEXT, "levelsTo[]");
		$levelsDonations = $form->getHttpData(Form::DATA_TEXT, "donations[]");
		$levelsData = array();

		
		
		foreach ($levelsFrom as $index => $levelFrom) {
			if (empty($levelsTo[$index]) || empty($levelsDonations[$index])) {
				continue;
			}
			if (!Validators::isNumericInt($levelsTo[$index]) || !Validators::isNumericInt($levelsDonations[$index])) {
				continue;
			}
			
			$levelsData[] = (object) array(
				"from" => (int) $levelFrom,
				"to" => (int) $levelsTo[$index],
				"donation" => (int) $levelsDonations[$index]
			);
		}
	
		$this->buildingPlanFacade->createPlan($this->userEntity, $values, $levelsData);
		$users = $this->userFacade->getUsersList();
		$usersCount = 0;
		
		// send mail message to all users that want to receive a notification
		$dateToObj = new Nette\Utils\DateTime($values->dateTo);
		$dateNow = new Nette\Utils\DateTime();
		if ($dateToObj > $dateNow) {  // send mail only when the plan is still active
			$from = $p->context->getParameters()["email"]["from"];
			$message = new MailMessage();
			$message
				->setFrom("GL Asijská restaurace <{$from}>")
				->setSubject("Plánovaná stavba")
				->setBody("Automatické upozornění na nově založený plán stavby.\n\nAsijská restaurace přeje hezký den.");
			
			foreach ($users as $user) {
				if ($user->email !== NULL && $user->settings->sendEmailWhenBuildingPlanCreated) {
					$message->addTo($user->email);
					$usersCount++;
				}
			}
			
			if ($usersCount > 0) {
				$mailer = new SendmailMailer();
				$mailer->send($message);
			}
		}
		
		$p->flashMessage("Plán byl přidán", "success");
		$p->redirect("this");
	}
	
	
	/**
	 * @return Form
	 */
	public function createSetSavedGold()
	{
		$form = new Form();
		$form->addText("sum", "Mám našetřeno: ")
			->setRequired("Nezadal jsi, kolik máš našetřeno")
			->setAttribute("size", 6);
		
		$form->addHidden("planId");
		
		$form->addSubmit("setSavedGold", "Uložit");
		
		$form->onSuccess[] = $this->setSavedGoldSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function setSavedGoldSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$plan = $this->buildingPlanFacade->setSavedGold($values->planId, $this->userEntity, $values->sum);
			$p->flashMessage("Hodnota byla uložena", "success");
		} catch (Nette\InvalidArgumentException $e) {}
			catch (UniqueConstraintViolationException $e) {}
		
		$p->redirect("this");
	}
	
	
	/**
	 * @return Form
	 */
	public function createEditPlan()
	{
		$form = new Form;
		$form->addSelect("building", "Budova")
			->setRequired();
			
		$form->addText("level", "Úroveň")
			->setRequired("Nezadal jsi úroveň")
			->addRule(Form::INTEGER, "Úroveň není číslo");
		
		$form->addText("discount", "Sleva (%)")
			->setRequired("Nezadal jsi slevu")
			->addRule(Form::INTEGER, "Sleva není číslo")
			->setDefaultValue(0);
			
		$form->addText("dateFrom", "Od")
			->setRequired("Nebylo zadáno datum od");
		
		$form->addText("dateTo", "Do")
			->setRequired("Nebylo zadáno datum do");
			
		$form->addTextarea("comment", "Komentář")
			->setAttribute("rows", 6)
			->setAttribute("cols", 50);
		
		$form->addHidden("planId");
		
		$form->addSubmit("create", "Upravit plán");
		$form->onSuccess[] = $this->editPlanSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function editPlanSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$plan = $this->buildingPlanFacade->editPlan($this->userEntity, $values->planId, $values->building, $values->level, $values->discount, $values->dateFrom, $values->dateTo, $values->comment);
			$p->flashMessage("Plán byl upraven", "success");
		} catch (Nette\InvalidArgumentException $e) {}
			catch (UniqueConstraintViolationException $e) {}
		
		$p->redirect("this");
	}
	
	
	/**
	 * @return Form
	 */
	public function createAddDonationInfo()
	{
		$form = new Form();
		$form->addSelect("user", "Hráč")
			->setRequired();
				
		$form->addText("amount", "Příspěvek")
			->setRequired("Nezadal jsi příspěvek")
			->addRule(Form::INTEGER, "Příspěvek není číslo")
			->setDefaultValue(0);
		
		$form->addHidden("planId");
		
		$form->addSubmit("addDonationInfo", "Přidat informaci");
		$form->onSuccess[] = $this->addDonationInfoSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function addDonationInfoSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$this->buildingPlanFacade->setUsersDonationInfo($values->planId, $this->userEntity, $values->user, $values->amount);
			$p->flashMessage("Informace byla přidána", "success");
			$p->redirect("this");
		} catch (UniqueConstraintViolationException $e) {
			$p->flashMessage("Tato informace je již uložena", "error");
		} catch (Nette\InvalidArgumentException $e) {
			$p->flashMessage($e->getMessage(), "error");
		}
		
		$p->redirect("this");
	}
	
	
	/**
	 * @return Form
	 */
	public function createSetDonation()
	{
		$form = new Form();
		$form->addText("amount", "Přispěl jsem: ")
			->setRequired("Nezadal jsi hodnotu příspěveku")
			->setAttribute("size", 6);
		
		$form->addHidden("planId");
		
		$form->addSubmit("setDonation", "Uložit");
		
		$form->onSuccess[] = $this->setDonationSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function setDonationSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$plan = $this->buildingPlanFacade->setDonationInfo($values->planId, $this->userEntity, $values->amount);
			$p->flashMessage("Hodnota byla uložena", "success");
		} catch (Nette\InvalidArgumentException $e) {}
			catch (UniqueConstraintViolationException $e) {}
		
		$p->redirect("this");
	}

}