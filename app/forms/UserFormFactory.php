<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Entities\UserDescription,
	App\Model\Facades\UserFacade;


class UserFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;

	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 * @param AbsenceFacade
	 */
	public function __construct(User $user, UserFacade $userFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createChangePassword()
	{
		$form = new Form();
		$form->addPassword("oldPassword", "Původní heslo")
			->setRequired("Nezadal jsi původní heslo");

		$form->addPassword("newPassword", "Nové heslo")
			->setRequired("Nezadal jsi nové heslo");
			
		$form->addPassword("verifyPassword", "Nové heslo znovu")
			->setRequired("Nezadal jsi ověření nového hesla");
		
		$form->addSubmit("changePassword", "Změnit heslo");
		$form->onSuccess[] = $this->changePasswordSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function changePasswordSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			if ($values->newPassword !== $values->verifyPassword) {
				throw new Nette\InvalidArgumentException("Zadaná hesla se neshodují");
			}
			
			$this->userFacade->changePassword($this->userEntity, $values->oldPassword, $values->newPassword);
			$p->flashMessage("Heslo bylo změněno", "success");
			$p->redirect("this");
		} catch(Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}
	
	
	/**
	 * @return Form
	 */
	public function createChangeEmail()
	{
		$form = new Form();
		$form->addText("email", "E-mail")
			->addRule(Form::EMAIL, "E-mail je ve špatném formátu");
		
		$form->addSubmit("changeEmail", "Upravit email");
		$form->onSuccess[] = $this->changeEmailSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function changeEmailSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		$this->userFacade->changeEmail($this->userEntity, $values->email);
		$p->flashMessage("E-mail byl změněn", "success");
		$p->redirect("this");
	}
	
	
	/**
	 * @return Form
	 */
	public function createChangeDescription()
	{
		$form = new Form();
		$form->addTextarea("content", "")
			->setAttribute("rows", 6)
			->setAttribute("cols", 50);
		
		$form->addSubmit("changeDescription", "Upravit popis");
		$form->onSuccess[] = $this->changeDescriptionSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function changeDescriptionSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		$this->userFacade->changeDescription($this->userEntity, $values->content);
		$p->flashMessage("Popis byl upraven", "success");
		$p->redirect("this");
	}
	
	
	/**
	 * @return Form
	 */
	public function createGeneralSettings()
	{
		$form = new Form();
		$form->addCheckbox("sendEmailWhenBuildingPlanCreated", "Odeslat mailové upozornění při vytvoření nového plánu na stavbu");
		$form->addCheckbox("levelPowerupActive", "Mám aktivovanou pečeť prétora (vidím na trhu o dvě úrovně výš)");
		
		$form->addSubmit("saveGeneralSettings", "Uložit nastavení");
		$form->onSuccess[] = $this->generalSettingsSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function generalSettingsSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		$this->userFacade->changeSettings($this->userEntity, $values);
		$p->flashMessage("Nastavení bylo uloženo", "success");
		$p->redirect("this");
	}

}