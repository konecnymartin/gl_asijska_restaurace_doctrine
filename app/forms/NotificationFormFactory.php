<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	App\Model\Facades\NotificationFacade;


class NotificationFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var NotificationFacade */
	private $notificationFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 * @param AbsenceFacade
	 */
	public function __construct(User $user, UserFacade $userFacade, NotificationFacade $notificationFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		$this->notificationFacade = $notificationFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createAdd()
	{
		$form = new Form();
			
		$form->addTextarea("content", "Oznámení")
			->setRequired("Nevyplnil jsi obsah")
			->setAttribute("rows", 4)
			->setAttribute("cols", 50);
		
		$form->addSubmit("addNotification", "Přidat oznámení");
		$form->onSuccess[] = $this->addSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function addSubmitted(Form $form, $values)
	{
		try {
			$this->notificationFacade->addNotification($this->userEntity, $values->content);
			$p = $form->getPresenter();
			$p->flashMessage("Oznámení bylo přidána", "success");
			$p->redirect("this");
		} catch(Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}

}