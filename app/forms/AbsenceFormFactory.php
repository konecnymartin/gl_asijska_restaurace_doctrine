<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	App\Model\Facades\AbsenceFacade;


class AbsenceFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var AbsenceFacade */
	private $absenceFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 * @param AbsenceFacade
	 */
	public function __construct(User $user, UserFacade $userFacade, AbsenceFacade $absenceFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		$this->absenceFacade = $absenceFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createAdd()
	{
		$form = new Form();
		$form->addText("from", "Od:")
			->setRequired("Nezadal jsi datum od");
		
		$form->addText("to", "Do:")
			->setRequired("Nezadal jsi datum do");
			
		$form->addTextarea("reason", "Důvod:")
			->setRequired("Nezadal jsi důvod absence")
			->setAttribute("rows", 4)
			->setAttribute("cols", 50);
		
		$form->addSubmit("addAbsence", "Přidat absenci");
		$form->onSuccess[] = $this->addSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function addSubmitted(Form $form, $values)
	{
		try {
			$this->absenceFacade->addAbsence($this->userEntity, $values->from, $values->to, $values->reason);
			$p = $form->getPresenter();
			$p->flashMessage("Absence byla přidána", "success");
			$p->redirect("this");
		} catch(Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		} catch (\Exception $e) {
			
			// when catching exception thrown by DateTime
			// if we get Nette\InvalidArgumentException, this catch block is also procceeded
			if (get_class($e) === "Exception") {
				$form->addError("Datum bylo zadáno ve špatném formátu");
			}
		}
	}

}