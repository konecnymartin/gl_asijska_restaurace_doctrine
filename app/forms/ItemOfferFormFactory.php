<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Entities\Item,
	App\Model\Entities\PrefixSuffix,
	App\Model\Facades\UserFacade,
	App\Model\Facades\ItemOfferFacade;


class ItemOfferFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var ItemOfferFacade */
	private $itemOfferFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 * @param AbsenceFacade
	 */
	public function __construct(User $user, UserFacade $userFacade, ItemOfferFacade $itemOfferFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		$this->itemOfferFacade = $itemOfferFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createOfferItem()
	{
		$form = new Form();
		$form->addText("prefix", "Prefix");
		$form->addText("itemName", "Předmět")
			->setRequired("Nevyplnil jsi název předmětu");
			
		$form->addText("suffix", "Suffix");
		$form->addText("minDamage", "")
			->setAttribute("size", 3);
			
		$form->addText("maxDamage")
			->setAttribute("size", 3);
			
		$form->addText("armour", "Zbroj")
			->setAttribute("size", 5);
		$form->addSelect("improvementType", "Zdokonalení", array(
			Item::IMPROVEMENT_NONE => "-- Vyberte --",
			Item::IMPROVEMENT_GRINDSTONE => "Brousek",
			Item::IMPROVEMENT_PROTECTIVE_GEAR => "Ochranné vybavení",
			Item::IMPROVEMENT_BLUE_POWDER => "Modrý prach",
			Item::IMPROVEMENT_YELLOW_POWDER => "Žlutý prach",
			Item::IMPROVEMENT_GREEN_POWDER => "Zelený prach",
			Item::IMPROVEMENT_ORANGE_POWDER => "Oranžový prach",
			Item::IMPROVEMENT_PURPLE_POWDER => "Fialový prach",
			Item::IMPROVEMENT_RED_POWDER => "Červený prach"
		));
		
		$form->addText("improvementValue", "")
			->setAttribute("size", 3);
			
		$form->addSelect("quality", "Kvalita", array(
			Item::QUALITY_BASIC => "Základní",
			Item::QUALITY_GREEN => "Zelená",
			Item::QUALITY_BLUE => "Modrá",
			Item::QUALITY_PINK => "Růžová",
			Item::QUALITY_ORANGE => "Oranžová",
			Item::QUALITY_RED => "Červená",
		))
			->setDefaultValue(Item::QUALITY_GREEN);
		
		$form->addText("price", "Cena")
			->addRule(Form::MIN, "Cena nesmí být záporná", 0)
			->setDefaultValue(0);
			
		$form->addSubmit("createOffer", "Vytvořit nabídku");
		
		$form->onSuccess[] = $this->offerItemSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function offerItemSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$this->itemOfferFacade->addOffer($this->userEntity, $values);
			$p->flashMessage("Nabídka byla vytvořena", "success");
			$p->redirect("this");
		} catch (Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}
	
	
	/**
	 * @return Form
	 */
	public function createEditOffer()
	{
		$form = new Form();
		$form->addText("prefix", "Prefix");
		$form->addText("itemName", "Předmět")
			->setRequired("Nevyplnil jsi název předmětu");
			
		$form->addText("suffix", "Suffix");
		$form->addText("minDamage", "")
			->setAttribute("size", 3);
			
		$form->addText("maxDamage")
			->setAttribute("size", 3);
			
		$form->addText("armour", "Zbroj")
			->setAttribute("size", 5);
		$form->addSelect("improvementType", "Zdokonalení", array(
			Item::IMPROVEMENT_NONE => "-- Vyberte --",
			Item::IMPROVEMENT_GRINDSTONE => "Brousek",
			Item::IMPROVEMENT_PROTECTIVE_GEAR => "Ochranné vybavení",
			Item::IMPROVEMENT_BLUE_POWDER => "Modrý prach",
			Item::IMPROVEMENT_YELLOW_POWDER => "Žlutý prach",
			Item::IMPROVEMENT_GREEN_POWDER => "Zelený prach",
			Item::IMPROVEMENT_ORANGE_POWDER => "Oranžový prach",
			Item::IMPROVEMENT_PURPLE_POWDER => "Fialový prach",
			Item::IMPROVEMENT_RED_POWDER => "Červený prach"
		));
		
		$form->addText("improvementValue", "")
			->setAttribute("size", 3);
			
		$form->addSelect("quality", "Kvalita", array(
			Item::QUALITY_BASIC => "Základní",
			Item::QUALITY_GREEN => "Zelená",
			Item::QUALITY_BLUE => "Modrá",
			Item::QUALITY_PINK => "Růžová",
			Item::QUALITY_ORANGE => "Oranžová",
			Item::QUALITY_RED => "Červená",
		))
			->setDefaultValue(Item::QUALITY_GREEN);
		
		$form->addText("price", "Cena")
			->addRule(Form::MIN, "Cena nesmí být záporná", 0)
			->setDefaultValue(0);
			
		$form->addHidden("offerId");
			
		$form->addSubmit("editOffer", "Upravit nabídku");
		
		$form->onSuccess[] = $this->editOfferSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function editOfferSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$this->itemOfferFacade->editOffer($this->userEntity, $values);
			$p->flashMessage("Nabídka byla upravena", "success");
			$p->redirect("this");
		} catch (Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}

}