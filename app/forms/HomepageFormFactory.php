<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade;


class HomepageFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 */
	public function __construct(User $user, UserFacade $userFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createLogin()
	{
		$form = new Form();
		$form->addText("name", "Jméno")
			->setRequired("Nebylo vyplněno jméno");

		$form->addPassword("password", "Heslo")
			->setRequired("Nebylo vyplněno heslo");

		$form->addSubmit("login", "Přihlásit se");
		
		$form->onSuccess[] = $this->loginSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function loginSubmitted($form, $values)
	{
		$this->user->setExpiration("14 days", FALSE);
		
		try {
			$this->user->login($values->name, $values->password);
			$user = $this->userFacade->getUser($this->user->id);
			$ip = $form->getPresenter()->context->getService("httpRequest")->getRemoteAddress();
			$this->userFacade->addLogin($user, $ip);
			
			$p = $form->getPresenter();
			$p->redirect("this");
		} catch (Nette\Security\AuthenticationException $e) {
			$form->addError($e->getMessage());
		}
	}
	

}