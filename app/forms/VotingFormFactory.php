<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	App\Model\Facades\VotingFacade;


class VotingFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var VotingFacade */
	private $votingFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 * @param VotingFacade
	 */
	public function __construct(User $user, UserFacade $userFacade, VotingFacade $votingFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		$this->votingFacade = $votingFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createCreateVoting()
	{
		$form = new Form();
		$form->addText("title", "Název")
			->setRequired("Nezadal jsi název");
		
		$form->addText("dateEnd", "Čas do");
		
		$form->addTextarea("content", "Obsah")
			->setAttribute("rows", 3)
			->setAttribute("cols", 60);
		
		$form->addSubmit("createVoting", "Vytvořit hlasování");
		$form->onSuccess[] = $this->createVotingSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function createVotingSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		$answers = $form->getHttpData(Form::DATA_TEXT, "answers[]");
		$answersCount = 0;
		
		foreach ($answers as $key => $answer) {
			if (empty($answer)) {
				unset($answers[$key]);
				continue;
			}
			
			$answersCount++;
		}
		
		try {
			if ($answersCount < 2) {
				throw new Nette\InvalidArgumentException("Musíš zadat minimálně dvě odpovědi");
			}
			
			$this->votingFacade->createVoting($this->userEntity, $values->title, $values->content, $values->dateEnd, $answers);
			$p->flashMessage("Hlasování bylo vytvořeno", "success");
			$p->redirect("this");
		} catch (Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}
	
	
	/**
	 * @return Form
	 */
	public function createVote()
	{
		$form = new Form();
		$form->addRadioList("answer", "")
			->setRequired("Nevybral jsi možnost");
		
		$form->addHidden("themeId");
		
		$form->addSubmit("vote", "Hlasovat");
		$form->onSuccess[] = $this->voteSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function voteSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			// when voting ends, form is set to be disabled and no data are sent
			if (!isset($form->values->answer)) {
				throw new Nette\InvalidArgumentException("Nelze hlasovat");
			}
			
			$this->votingFacade->addVote($values->themeId, $this->userEntity, $form->values->answer);
			$p->flashMessage("Hlas byl zaznamenán", "success");
			$p->redirect("this");
		} catch (Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}
	
	
	/**
	 * @return Form
	 */
	public function createAddComment()
	{
		$form = new Form();
		$form->addTextarea("content", "Komentář")
			->setRequired("Nevyplnil jsi komentář")
			->setAttribute("rows", 4)
			->setAttribute("cols", 45);
		
		$form->addHidden("themeId");
		
		$form->addSubmit("addComment", "Přidat komentář");
		$form->onSuccess[] = $this->addCommentSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\ArrayHash
	 */
	public function addCommentSubmitted(Form $form, $values)
	{
		$p = $form->getPresenter();
		try {
			$this->votingFacade->addComment($values->themeId, $this->userEntity, $values->content);
			$p->flashMessage("Komentář byl uložen", "success");
			$p->redirect("this");
		} catch (Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}

}