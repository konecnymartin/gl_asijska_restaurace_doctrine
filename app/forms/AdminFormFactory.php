<?php

namespace App\Forms;

use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User,
	\Doctrine\ORM\NoResultException,
	App\Model\Entities\User as UserEntity,
	App\Model\Facades\UserFacade,
	Doctrine\DBAL\Exception\UniqueConstraintViolationException,
	App\Model\Entities\ViewPermissions;


class AdminFormFactory extends Nette\Object
{

	/** @var User */
	private $user;
	
	/** @var UserFacade */
	private $userFacade;
	
	/** @var UserEntity */
	private $userEntity;
	
	
	/**
	 * @param User
	 * @param UserFacade
	 */
	public function __construct(User $user, UserFacade $userFacade)
	{
		$this->user = $user;
		$this->userFacade = $userFacade;
		
		$this->userEntity = $userFacade->getUser($user->id);
	}
	
	
	/**
	 * @return Form
	 */
	public function createAddUser()
	{
		$form = new Form();
		$form->addText("name", "Jméno")
			->setRequired("Nezadal jsi jméno");

		$form->addText("password", "Heslo")
			->setRequired("Nezadal jsi heslo");
		
		$form->addText("gladiatusId", "Gladiatus ID")
			->setRequired("Nezadal jsi Gladiatus ID");
		
		$form->addRadioList("role", "Hodnost", array(
			UserEntity::ROLE_USER => "uživatel",
			UserEntity::ROLE_ADMIN => "admin"
		))
			->setRequired();
		
		$form->addCheckbox("hidden", "Skrytý");
		$form->addCheckbox("inGuild", "V gildě");
		
		$form->addCheckboxList("viewPermissions", "Práva zobrazení", array(
			ViewPermissions::RULES => "Pravidla",
			ViewPermissions::ABOUT_MEMBERS => "O členech",
			ViewPermissions::HINTS => "Tipy a triky ke hře",
			ViewPermissions::BUILDING_PLANS => "Plány staveb",
			ViewPermissions::VOTINGS => "Hlasování",
			ViewPermissions::ITEM_OFFERS => "Nabídky předmětů",
			ViewPermissions::ABSENCES => "Absence",
			ViewPermissions::CONTACT => "Kontakt",
			ViewPermissions::NOTIFICATIONS => "Oznámení"
		));
		
		$form->addSubmit("addUser", "Přidat uživatele");
		$form->onSuccess[] = $this->addUserSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param Nette\Utils\ArrayHash
	 */
	public function addUserSubmitted(Form $form, $values)
	{
		try {
			$this->userFacade->addUser($this->userEntity, $values);
			$p = $form->getPresenter();
			$p->flashMessage("Uživatel byl přidán", "success");
			$p->redirect("this");
		} catch (UniqueConstraintViolationException $e) {
			$form->addError("Tento uživatel již existuje");
		}
	}
	
	
	/**
	 * @return Form
	 */
	public function createEditUser()
	{
		$form = new Form();
		$form->addText("name", "Jméno")
			->setRequired("Nebylo vyplněno jméno");
		
		$form->addText("newPassword", "Nové heslo");

		$form->addText("gladiatusId", "Gladiatus ID");
		
		$form->addRadioList("role", "Hodnost", array(
			UserEntity::ROLE_USER => "uživatel",
			UserEntity::ROLE_ADMIN => "admin"
		))
			->setRequired();
		
		$form->addCheckbox("hidden", "Skrytý");
		$form->addCheckbox("inGuild", "V gildě");
		
		$form->addCheckboxList("viewPermissions", "Práva zobrazení", array(
			ViewPermissions::RULES => "Pravidla",
			ViewPermissions::ABOUT_MEMBERS => "O členech",
			ViewPermissions::HINTS => "Tipy a triky ke hře",
			ViewPermissions::BUILDING_PLANS => "Plány staveb",
			ViewPermissions::VOTINGS => "Hlasování",
			ViewPermissions::ITEM_OFFERS => "Nabídky předmětů",
			ViewPermissions::ABSENCES => "Absence",
			ViewPermissions::CONTACT => "Kontakt",
			ViewPermissions::NOTIFICATIONS => "Oznámení"
		));
		
		$form->addTextarea("description", "Popis")
			->setAttribute("rows", 4)
			->setAttribute("cols", 60);
		
		$form->addHidden("userId");

		$form->addSubmit("editUser", "Upravit uživatele");
		
		$form->onSuccess[] = $this->editUserSubmitted;
		
		return $form;
	}
	
	
	/**
	 * @param Form
	 * @param \stdClass
	 */
	public function editUserSubmitted($form, $values)
	{
		try {
			$this->userFacade->editUser($this->userEntity, $values);
			
			if (!empty($values->newPassword)) {
				$this->userFacade->changePasswordAdmin($this->userEntity, $values->userId, $values->newPassword);
			}
			
			$p = $form->getPresenter();
			$p->flashMessage("Uživatel byl upraven", "success");
			$p->redirect("this");
		} catch (Nette\InvalidArgumentException $e) {
			$form->addError($e->getMessage());
		}
	}
	

}