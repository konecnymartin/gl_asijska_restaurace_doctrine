<?php

namespace Traits;

use App\Model\Entities\ItemOffer,
	App\Model\Entities\Item,
	App\Model\Entities\User,
	Nette\Utils\Html;

trait ItemTooltipHelper
{

	/**
	 * @param ItemOffer
	 * @param User   when we want to calculate percentual attributes for user
	 * @return string
	 */
	public function helperGenerateItemOfferTooltip(ItemOffer $offer, User $user = NULL)
	{
		$attrs = $offer->getItemAttributes();
		$item = $offer->item;
		
		// 1) attributes
		$attrsTooltip = array(
			"damage" => array(
				"name" => "Poškození",
				"value" => $attrs->damage,
				"percentually" => 0
			),
			"armour" => array(
				"name" => "Zbroj",
				"value" => $attrs->armour,
				"percentually" => 0
			),
			"health" => array(
				"name" => "Zdraví",
				"value" => $attrs->health,
				"percentually" => 0
			),
			"strength" => array(
				"name" => "Síla",
				"value" => $attrs->strength,
				"percentually" => $attrs->strengthPercentually
			),
			"skill" => array(
				"name" => "Dovednost",
				"value" => $attrs->skill,
				"percentually" => $attrs->skillPercentually
			),
			"agility" => array(
				"name" => "Obratnost",
				"value" => $attrs->agility,
				"percentually" => $attrs->agilityPercentually
			),
			"constitution" => array(
				"name" => "Odolnost",
				"value" => $attrs->constitution,
				"percentually" => $attrs->constitutionPercentually
			),
			"charisma" => array(
				"name" => "Charisma",
				"value" => $attrs->charisma,
				"percentually" => $attrs->charismaPercentually
			),
			"intelligence" => array(
				"name" => "Inteligence",
				"value" => $attrs->intelligence,
				"percentually" => $attrs->intelligencePercentually
			),
			"criticalStrike" => array(
				"name" => "Kritický útok",
				"value" => $attrs->criticalStrike,
				"percentually" => 0
			),
			"blocking" => array(
				"name" => "Blokování",
				"value" => $attrs->blocking,
				"percentually" => 0
			),
			"resilience" => array(
				"name" => "Pružnost",
				"value" => $attrs->resilience,
				"percentually" => 0
			),
			"threat" => array(
				"name" => "Ohrožení",
				"value" => $attrs->threat,
				"percentually" => 0
			),
			"healing" => array(
				"name" => "Léčení",
				"value" => $attrs->healing,
				"percentually" => 0
			),
			"criticalHealing" => array(
				"name" => "Kritické léčení",
				"value" => $attrs->criticalHealing,
				"percentually" => 0
			)
		);
		
		
		// attributes in bracket due to percentual attributes
		$attrsBrackets = array(
			"strength" => 0,
			"skill" => 0,
			"agility" => 0,
			"constitution" => 0,
			"charisma" => 0,
			"intelligence" => 0
		);
		
		if ($user !== NULL) {
			$userData = $user->getGladiatusData();
			foreach ($attrsBrackets as $key => &$value) {
				$value = round($attrsTooltip[$key]["percentually"] / 100 * $userData->$key);
			}
		}
		
		
		if ($item->type == Item::TYPE_WEAPON) {
			$attrsTooltip["damage"]["value"] = "{$offer->itemMinDamage} - {$offer->itemMaxDamage}";
		}
		if ($item->type != Item::TYPE_RING && $item->type != Item::TYPE_AMULET && $item->type != Item::TYPE_WEAPON) {
			$attrsTooltip["armour"]["value"] = $offer->itemArmour;
		}
		
		switch ($offer->itemQuality) {
			case Item::QUALITY_BASIC:
				$shadow = "";
				$color = "white";
				break;
		
			case Item::QUALITY_GREEN:
				$shadow = "";
				$color = "lime";
				break;
				
			case Item::QUALITY_BLUE:
				$shadow = "0 0 2px #000, 0 0 2px #5159F7";
				$color = "#5159F7";
				break;
				
			case Item::QUALITY_PINK:
				$shadow = "0 0 2px #000, 0 0 4px #E303E0";
				$color = "#E303E0";
				break;
				
			case Item::QUALITY_ORANGE:
				$shadow = "0 0 2px #000, 0 0 6px #FF6A00";
				$color = "#FF6A00";
				break;
			
			case Item::QUALITY_RED:
				$shadow = "0 0 2px #000, 0 0 6px #FF0000";
				$color = "#FF0000";
				break;
		}
		
		$tooltip = Html::el("div");
		$tooltip->class = "tooltip";

		$table = Html::el("table");
		$table->class = "tooltipBox";
		$tr = $table->create("tr");
		$td = $tr->create("td");
		$td->style["font-size"] = "9pt";
		$td->style["font-weight"] = "bold";
		$td->style["color"] = $color;
		$td->style["text-shadow"] = $shadow;
		$td->setText($offer->getFullItemName());
		

	
		foreach ($attrsTooltip as $attrName => $attrData) {
			$attrData = (object) $attrData;
			
			if ($attrData->value != 0) {
				if ($attrData->value > 0) {
					if (($item->type != Item::TYPE_WEAPON || $attrName !== "damage")) {
						$attrData->value = "+{$attrData->value}";
					}
				}

				$tr = $table->create("tr");
				$td = $tr->create("td");
				$td->style["font-size"] = "8pt";
				$td->style["color"] = "#DDD";
				$td->style["font-weight"] = "bold";
				$td->colspan = 2;
				$td->nowrap = "nowrap";
				$td->setText($attrData->name . " " . $attrData->value);
			}
			
			if ($attrData->percentually != 0) {
				if ($attrData->percentually > 0) {
					$attrData->percentually = "+{$attrData->percentually}";
				}
		
				$tr = $table->create("tr");
				$td = $tr->create("td");
				$td->style["font-size"] = "8pt";
				$td->style["color"] = "#DDD";
				$td->style["font-weight"] = "bold";
				$td->colspan = 2;
				$td->nowrap = "nowrap";
				
				$text = $attrData->name . " " . $attrData->percentually . "%";
				if ($attrData->percentually != 0 && $attrsBrackets[$attrName] != 0) {
					$prefix = $attrData->percentually > 0 ? "+" : "";
					$text .= " ({$prefix}{$attrsBrackets[$attrName]})";
				}
				
				$td->setText($text);
			}
		}
		
		
		// 2) improvements
		if ($offer->itemImprovementType != 0 && $offer->itemImprovementValue != 0) {
			switch ($offer->itemImprovementType) {
				case Item::IMPROVEMENT_GRINDSTONE:
					$improvementText = "poškození";
					break;
					
				case Item::IMPROVEMENT_PROTECTIVE_GEAR:
					$improvementText = "zbroj";
					break;
				
				case Item::IMPROVEMENT_BLUE_POWDER:
					$improvementText = "síla";
					break;
					
				case Item::IMPROVEMENT_YELLOW_POWDER:
					$improvementText = "dovednost";
					break;
					
				case Item::IMPROVEMENT_GREEN_POWDER:
					$improvementText = "obratnost";
					break;
					
				case Item::IMPROVEMENT_ORANGE_POWDER:
					$improvementText = "odolnost";
					break;
					
				case Item::IMPROVEMENT_PURPLE_POWDER:
					$improvementText = "charisma";
					break;
					
				case Item::IMPROVEMENT_RED_POWDER:
					$improvementText = "inteligence";
					break;
			}
			
			$tr = $table->create("tr");
			$td = $tr->create("td");
			$td->style["font-size"] = "8pt";
			$td->style["color"] = "#00B712;";
			$td->style["font-weight"] = "bold";
			$td->colspan = 2;
			$td->nowrap = "nowrap";
			$td->setText("+{$offer->itemImprovementValue} {$improvementText}");
		}
		
		
		// 3) level
		$tr = $table->create("tr");
		$td = $tr->create("td");
		$td->style["font-size"] = "8pt";
		$td->style["color"] = "#808080";
		$td->style["font-weight"] = "bold";
		$td->colspan = 2;
		$td->nowrap = "nowrap";
		$td->setText("Úroveň " . $attrs->level);
		
		
		// 4) price
		$tr = $table->create("tr");
		$td = $tr->create("td");
		$td->style["font-size"] = "8pt";
		$td->style["color"] = "#DDD";
		$td->style["font-weight"] = "bold";
		$td->colspan = 2;
		$td->nowrap = "nowrap";
		
		$priceText = "Cena ";
		if ($offer->price > 0) {
			$priceText .= number_format($offer->price, 0, ' ', '.') . " zlata";
		} else {
			$priceText .= "neuvedena";
		}
		
		$td->setText($priceText);	
		
		// 5) add table to div
		$tooltip->add($table);
		
		return $tooltip;
	}

}