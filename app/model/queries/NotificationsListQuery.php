<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	App\Model\Entities\User,
	App\Model\Entities\Notification,
	App\Model\Entities\UserHiddenNotification;

class NotificationsListQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("n, u")
			->from(Notification::class, "n")
			->leftJoin("n.user", "u");
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * TODO: make better queries, the thing with empty $hiddens array is a bit weird
	 *
	 * @param User
	 */
	public function byUser(User $user)
	{
		$this->filters[] = function(QueryBuilder $qb) use ($user) {
			$hiddensResult = $qb->getEntityManager()->createQueryBuilder()
				->select("hn")
				->from(UserHiddenNotification::class, "hn")
				->where("hn.user = :user", $user)
				->getQuery()
				->getResult();
			
			$hiddens = array();
			foreach ($hiddensResult as $hidden) {
				$hiddens[] = $hidden->notification->id;
			}
			
			// when there are no results, NULL is set to IN and the query doesn't work
			if (empty($hiddens)) {
				$hiddens[] = 0;
			}
			
			$qb->where("n.id NOT IN (:hiddens)", $hiddens);
		};
		
		return $this;
	}
	
	
	/**
	 * @param string
	 */
	public function orderByDate($seq)
	{
		if ($seq !== "ASC" && $seq !== "DESC") {
			$seq = "DESC";
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($seq) {
			$qb->addOrderBy("n.date", $seq);
		};
		
		return $this;
	}

}