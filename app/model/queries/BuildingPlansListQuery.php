<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	Nette,
	App\Model\Entities\User,
	App\Model\Entities\BuildingPlan;

class BuildingPlansListQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("bp")
			->from(BuildingPlan::class, "bp");
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * @param string
	 */
	public function orderByDate($seq)
	{
		if ($seq !== "ASC" && $seq !== "DESC") {
			$seq = "ASC";
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($seq) {
			$qb->addOrderBy("bp.from", $seq);
		};
		
		return $this;
	}
	
	
	/**
	 * @param int
	 */
	public function limit($num)
	{
		$num = (int) $num;
		$this->filters[] = function(QueryBuilder $qb) use ($num) {
			$qb->setMaxResults($num);
		};
		
		return $this;
	}
	
	

	public function onlyActual()
	{
		$this->filters[] = function(QueryBuilder $qb) {
			$qb->where("bp.to > :date", new Nette\Utils\DateTime());
		};
		
		return $this;
	}

}