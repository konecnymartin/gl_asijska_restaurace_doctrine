<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	App\Model\Entities\User,
	App\Model\Entities\Absence;

class AbsencesListQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("a")
			->from(Absence::class, "a");
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * @param array
	 */
	public function withoutUsers(array $users)
	{
		if (empty($users)) {
			return $this;
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($users) {
			//$qb->where($qb->expr()->notIn("a.user", $users));
			$qb->where("a.user NOT IN (:users)")
				->setParameter("users", $users);
		};
		
		return $this;
	}

}