<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	App\Model\Entities\User;

class UsersListQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $selects = array();

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("u")
			->from(User::class, "u");
		
		foreach ($this->selects as $select) {
			$select($qb);
		}
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * @param string
	 */
	public function orderByName($seq = "ASC")
	{
		if ($seq !== "ASC" && $seq !== "DESC") {
			$seq = "ASC";
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($seq) {
			$qb->addOrderBy("u.name", $seq);
		};
		
		return $this;
	}
	
	
	public function onlyVisible()
	{
		$this->filters[] = function(QueryBuilder $qb) {
			$qb->where("u.hidden = :hidden", FALSE);
		};
		
		return $this;
	}
	
	
	public function withItemOffersCount()
	{
		$this->selects[] = function(QueryBuilder $qb) {
			$qb->addSelect("COUNT(io) AS offersCount")
				->leftJoin("u.itemOffers", "io")
				->groupBy("u.id");
		};
		
		return $this;
	}
	
	
	public function inGuild()
	{
		$this->filters[] = function(QueryBuilder $qb) {
			$qb->where("u.inGuild = :inGuild", TRUE);
		};
		
		return $this;
	}
	
	
	public function notInGuild()
	{
		$this->filters[] = function(QueryBuilder $qb) {
			$qb->where("u.inGuild = :inGuild", FALSE);
		};
		
		return $this;
	}
	
	
	public function withDescription()
	{
		$this->selects[] = function(QueryBuilder $qb) {
			$qb->addSelect("d")
				->innerJoin("u.description", "d", "WITH");
		};
		
		return $this;
	}

}