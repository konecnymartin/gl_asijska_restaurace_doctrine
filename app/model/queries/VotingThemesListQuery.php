<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	Nette,
	App\Model\Entities\User,
	App\Model\Entities\VotingTheme,
	App\Model\Entities\VotingAnswer,
	App\Model\Entities\VotingVote;

class VotingThemesListQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("vt")
			->from(VotingTheme::class, "vt");
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * @param string
	 */
	public function orderByDate($seq)
	{
		if ($seq !== "ASC" && $seq !== "DESC") {
			$seq = "DESC";
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($seq) {
			$qb->addOrderBy("vt.dateCreated", $seq);
		};
		
		return $this;
	}
	
	
	/**
	 * @param User
	 */
	public function userNotVotedIn(User $user)
	{
		// TODO:
		// this is a quite weird way and it shouled be cleaner
		// but for now it works with these three queries
	
		$this->filters[] = function(QueryBuilder $qb) use ($user) {
			$votings = $qb->getEntityManager()->createQueryBuilder()
				->select("vv")
				->from(VotingVote::class, "vv")
				->where("vv.user = :user")
				->setParameter("user", $user);
			$answersArr = array();
			foreach ($votings as $row) {
				$voting = $row[0];
				$answersArr[] = $voting->answer;
			}
			
		
			$answers = $qb->getEntityManager()->createQueryBuilder()
				->select("a")
				->from(VotingAnswer::class, "a")
				->where("a.id IN (:answers)")
				->setParameter("answers", $answersArr)
				->groupBy("a");
			
			$themesArr = array();
			foreach ($answers as $row) {
				$answer = $row[0];
				$themesArr[] = $answer->theme;
			}
		
			$qb->where("vt NOT IN (:votedThemes) AND vt.dateEnd > :now")
				->setParameter("votedThemes", $themesArr)
				->setParameter("now", new Nette\Utils\DateTime());
		};
		
		return $this;
	}

}