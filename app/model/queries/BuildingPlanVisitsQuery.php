<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	Nette,
	App\Model\Entities\User,
	App\Model\Entities\BuildingPlan,
	App\Model\Entities\BuildingPlanVisit;

class BuildingPlanVisitsQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("v")
			->from(BuildingPlanVisit::class, "v");
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * @param BuildingPlan
	 */
	public function byPlan(BuildingPlan $plan)
	{
		$this->filters[] = function(QueryBuilder $qb) use ($plan) {
			$qb->andWhere("v.plan = :plan")
				->setParameter("plan", $plan);
		};
		
		return $this;
	}
	
	
	/**
	 * @param string
	 */
	public function orderByDate($seq)
	{
		if ($seq !== "ASC" && $seq !== "DESC") {
			$seq = "ASC";
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($seq) {
			$qb->addOrderBy("v.date", $seq);
		};
		
		return $this;
	}
	

	public function distinctUsers()
	{
		$this->filters[] = function(QueryBuilder $qb) {
			$qb->groupBy("v.user");
		};
		
		return $this;
	}

}