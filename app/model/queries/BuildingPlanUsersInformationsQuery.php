<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	Nette,
	App\Model\Entities\User,
	App\Model\Entities\BuildingPlan,
	App\Model\Entities\BuildingPlanUserInformations;

class BuildingPlanUsersInformationsQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var BuildingPlan */
	private $buildingPlan;

	/** @var array */
	private $filters = array();
	
	
	/**
	 * @param BuildingPlan
	 */
	public function __construct(BuildingPlan $plan)
	{
		$this->buildingPlan = $plan;
	}
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("ui, u")
			->from(BuildingPlanUserInformations::class, "ui")
			->innerJoin("ui.user", "u", "WITH")
			->where("ui.plan = :plan", $this->buildingPlan)
			->andWhere("u.hidden = :hidden", FALSE);
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	

	public function onlyGuild()
	{
		$this->filters[] = function(QueryBuilder $qb) {
			$qb->andWhere("u.inGuild = :inGuild", TRUE);
		};
		
		return $this;
	}

}