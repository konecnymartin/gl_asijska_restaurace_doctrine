<?php

namespace App\Model\Queries;

use \Doctrine\ORM\QueryBuilder,
	Nette,
	App\Model\Entities\User,
	App\Model\Entities\ItemOffer;

class ItemOffersListQuery extends \Kdyby\Doctrine\QueryObject
{

	/** @var array */
	private $filters = array();
	

	/**
	 * @param \Kdyby\Persistence\Queryable
	 * @return Doctrine\ORM\QueryBuilder
	 */
	public function doCreateQuery(\Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
			->select("io, u")
			->from(ItemOffer::class, "io")
			->innerJoin("io.user", "u", "WITH");
	
		foreach ($this->filters as $filter) {
			$filter($qb);
		}
		
		return $qb;
	}
	
	
	/**
	 * @param string
	 */
	public function orderByDate($seq)
	{
		if ($seq !== "ASC" && $seq !== "DESC") {
			$seq = "DESC";
		}
		
		$this->filters[] = function(QueryBuilder $qb) use ($seq) {
			$qb->addOrderBy("io.dateAdded", $seq);
		};
		
		return $this;
	}
	
	
	/**
	 * @param int
	 */
	public function limit($num)
	{
		$num = (int) $num;
		$this->filters[] = function(QueryBuilder $qb) use ($num) {
			$qb->setMaxResults($num);
		};
		
		return $this;
	}

}