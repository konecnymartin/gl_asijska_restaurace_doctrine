<?php

namespace App\Model\Facades;

use Nette,
	Kdyby\Doctrine\EntityManager,
	Kdyby\Doctrine\ResultSet,
	App\Model\Entities\User,
	App\Model\Entities\ItemOffer,
	App\Model\Entities\PrefixSuffix,
	App\Model\Queries\UsersListQuery,
	App\Model\Queries\ItemOffersListQuery;


class ItemOfferFacade
{

	/** @var EntityManager */
	private $em;
	
	
	/**
	 * @param EntityManager
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	
	/**
	 * @param int|NULL
	 * @param bool
	 * @return ItemOffer|NULL
	 * @throws Nette\InvalidArgumentException
	 */
	public function getOffer($id, $throwException = FALSE)
	{
		$offer = NULL;
		if ($id !== NULL) {
			$offer = $this->em->find(ItemOffer::class, $id);
		}
		
		if ($offer === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Taková nabídka neexistuje");
		}
		
		return $offer;
	}
	

	/**
	 * @return ResultSet
	 */
	public function getUsersWithOffersCount()
	{
		$query = new UsersListQuery();
		$query->onlyVisible()
			->orderByName("ASC")
			->withItemOffersCount();
		
		return $this->em->getRepository(User::class)->fetch($query);
	}
	
	
	/**
	 * @return ResultSet
	 */
	public function getOffers()
	{
		$query = new ItemOffersListQuery();
		$query->orderByDate("DESC");
		
		return $this->em->getRepository(ItemOffer::class)->fetch($query);
	}

	
	/**
	 * @param User
	 * @param Nette\ArrayHash
	 */
	public function addOffer(User $user, $values)
	{
		$offer = new ItemOffer();
		$prefix = NULL;
		$suffix = NULL;
		
		if (!empty($values->prefix)) {
			$prefix = $this->getPrefixByName($values->prefix, TRUE);
		}
		if (!empty($values->suffix)) {
			$suffix = $this->getSuffixByName($values->suffix, TRUE);
		}
		
		$item = $this->getItemByName($values->itemName, TRUE);
		$offer->itemQuality = $values->quality;
		$offer->itemMinDamage = !empty($values->minDamage) ? $values->minDamage : NULL;
		$offer->itemMaxDamage = !empty($values->maxDamage) ? $values->maxDamage : NULL;
		$offer->itemArmour = !empty($values->armour) ? $values->armour : NULL;
		$offer->itemImprovementType = !empty($values->improvementType) ? $values->improvementType : NULL;
		$offer->itemImprovementValue = !empty($values->improvementValue) ? $values->improvementValue : NULL;
		$offer->price = !empty($values->price) ? (int) $values->price : 0;
		$offer->item = $item;
		$offer->prefix = $prefix;
		$offer->suffix = $suffix;
		$offer->dateAdded = new Nette\Utils\DateTime();
		
		$user->addItemOffer($offer);
		$this->em->flush();
	}
	
	
	/**
	 * @param string
	 * @param bool
	 * @return PrefixSuffix
	 * @throws Nette\InvalidArgumentException
	 */
	public function getPrefixByName($name, $throwException = FALSE)
	{
		$prefix = $this->em->createQuery("
			SELECT p
			FROM App\Model\Entities\PrefixSuffix p
			WHERE p.name LIKE :name AND p.type = :type
		")
			->setParameter("name", $name)
			->setParameter("type", PrefixSuffix::PREFIX)
			->getOneOrNullResult();
		
		if ($prefix === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takový prefix neexistuje");
		}
		
		return $prefix;
	}
	
	
	/**
	 * @param string
	 * @param bool
	 * @return PrefixSuffix
	 * @throws Nette\InvalidArgumentException
	 */
	public function getSuffixByName($name, $throwException = FALSE)
	{
		$suffix = $this->em->createQuery("
			SELECT p
			FROM App\Model\Entities\PrefixSuffix p
			WHERE p.name LIKE :name AND p.type = :type
		")
			->setParameter("name", $name)
			->setParameter("type", PrefixSuffix::SUFFIX)
			->getOneOrNullResult();
		
		if ($suffix === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takový suffix neexistuje");
		}
		
		return $suffix;
	}
	
	
	/**
	 * @param string
	 * @param bool
	 * @return Item
	 * @throws Nette\InvalidArgumentException
	 */
	public function getItemByName($name, $throwException = FALSE)
	{
		$item = $this->em->createQuery("
			SELECT i
			FROM App\Model\Entities\Item i
			WHERE i.name LIKE :name
		")
			->setParameter("name", $name)
			->getOneOrNullResult();
		
		if ($item === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takový předmět neexistuje");
		}
		
		return $item;
	}
	
	
	/**
	 * @param int
	 * @return ResultSet
	 */
	public function getLatestOffers($num)
	{
		$query = new ItemOffersListQuery();
		$query->orderByDate("DESC")
			->limit($num);
		
		return $this->em->getRepository(ItemOffer::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function deleteOffer(User $user, $offerId)
	{
		$offer = $user->getItemOffer($offerId);
		if ($offer === NULL) {
			throw new Nette\InvalidArgumentException("Tato nabídka neexistuje");
		}
		
		$this->em->remove($offer);
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function editOffer(User $user, $values)
	{
		$offer = $user->getItemOffer($values->offerId);
		if ($offer === NULL) {
			throw new Nette\InvalidArgumentException("Nemůžeš upravit tuto nabídku");
		}
		
		$prefix = NULL;
		$suffix = NULL;
		
		if (!empty($values->prefix)) {
			$prefix = $this->getPrefixByName($values->prefix, TRUE);
		}
		if (!empty($values->suffix)) {
			$suffix = $this->getSuffixByName($values->suffix, TRUE);
		}
		
		$item = $this->getItemByName($values->itemName, TRUE);
		$offer->itemQuality = $values->quality;
		$offer->itemMinDamage = !empty($values->minDamage) ? $values->minDamage : NULL;
		$offer->itemMaxDamage = !empty($values->maxDamage) ? $values->maxDamage : NULL;
		$offer->itemArmour = !empty($values->armour) ? $values->armour : NULL;
		$offer->itemImprovementType = !empty($values->improvementType) ? $values->improvementType : NULL;
		$offer->itemImprovementValue = !empty($values->improvementValue) ? $values->improvementValue : NULL;
		$offer->price = !empty($values->price) ? (int) $values->price : 0;
		
		if ($offer->item !== $item) {
			$offer->item = $item;
		}
		if ($offer->prefix !== $prefix) {
			$offer->prefix = $prefix;
		}
		if ($offer->suffix !== $suffix) {
			$offer->suffix = $suffix;
		}
		
		$this->em->flush();
	}
	
}