<?php

namespace App\Model\Facades;

use Nette,
	Kdyby\Doctrine\EntityManager,
	Kdyby\Doctrine\ResultSet,
	App\Model\Facades\UserFacade,
	App\Model\Entities\User,
	App\Model\Entities\BuildingPlan,
	App\Model\Entities\Building,
	App\Model\Entities\BuildingPlanLevelDonation,
	App\Model\Entities\BuildingPlanUserInformations,
	App\Model\Entities\BuildingPlanDonation,
	App\Model\Entities\BuildingPlanVisit,
	App\Model\Queries\BuildingPlansListQuery,
	App\Model\Queries\BuildingPlanUsersInformationsQuery,
	App\Model\Queries\BuildingPlanVisitsQuery;


class BuildingPlanFacade
{

	/** @var EntityManager */
	private $em;
	
	/** @var UserFacade */
	private $userFacade;
	
	
	/**
	 * @param EntityManager
	 */
	public function __construct(EntityManager $em, UserFacade $userFacade)
	{
		$this->em = $em;
		$this->userFacade = $userFacade;
	}
	
	
	/**
	 * @param int|NULL
	 * @param bool
	 * @return BuildingPlan|NULL
	 * @throws Nette\InvalidArgumentException
	 */
	public function getPlan($id, $throwException = FALSE)
	{
		$buildingPlan = NULL;
		if ($id !== NULL) {
			$buildingPlan = $this->em->find(BuildingPlan::class, $id);
		}
		
		if ($buildingPlan === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takový plán neexistuje");
		}
		
		return $buildingPlan;
	}
	
	
	/**
	 * @return ResultSet
	 */
	public function getPlans()
	{
		$query = new BuildingPlansListQuery();
		$query->orderByDate("DESC");

		return $this->em->getRepository(BuildingPlan::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param Nette\ArrayHash
	 * @param array
	 * @throws Nette\InvalidArgumentException
	 */
	public function createPlan(User $user, $data, $levelsDonations)
	{
		if (!isset($data->building)) {
			throw new Nette\InvalidArgumentException("Nebyla vybrána budova");
		}
		if (!isset($data->level)) {
			throw new Nette\InvalidArgumentException("Nebyla zadána úroveň");
		}
		if (!isset($data->dateTo)) {
			throw new Nette\InvalidArgumentException("Nebyl zadán datum konce stavby");
		}
		if (!isset($data->usersInformations)) {
			throw new Nette\InvalidArgumentException("Nebyly zadány informace pro uživatele");
		}
		if (!isset($data->dateFrom)) {
			$data->from = new Nette\Utils\DateTime();
		}
		if (!isset($data->discount)) {
			$data->discount = 0;
		}
		
		$optional = isset($data->optional) && $data->optional;
		
		$building = new Building();
		$building->key = $data->building;
		$building->level = $data->level;
		$building->percentualDiscount = $data->discount;
		
		$plan = new BuildingPlan();
		$plan->from = is_object($data->dateFrom) ? $data->from : new Nette\Utils\DateTime($data->dateFrom);
		$plan->to = is_object($data->dateTo) ? $data->to : new Nette\Utils\DateTime($data->dateTo);
		$plan->building = $building;
		$plan->comment = $data->comment;
		$plan->optional = $optional;
		$plan->user = $user;
		
		$users = $this->userFacade->getUsersList();
		foreach ($users as $user) {
			$donationRequired = FALSE;  // default for hidden users (still needed to add a record)
			
			foreach ($data->usersInformations as $userId => $information) {
				if ($user->id == $userId) {
					$donationRequired = $information->donationRequired;
					break;
				}
			}
			
			$info = new BuildingPlanUserInformations();
			$info->donationRequired = $donationRequired;
			$info->savedGold = 0;
			$info->user = $user;
			$plan->addUserInformations($info);
		}
		
		foreach ($levelsDonations as $level) {
			$donation = new BuildingPlanLevelDonation();
			$donation->levelFrom = $level->from;
			$donation->levelTo = $level->to;
			$donation->donation = $level->donation;			
			$plan->addLevelDonation($donation);
		}
		
		$this->em->persist($building);
		$this->em->persist($plan);
		$this->em->flush();
	}
	
	
	/**
	 * @param int
	 */
	public function getActualPlans($num = 1)
	{
		$query = new BuildingPlansListQuery();
		$query->orderByDate("DESC")
			->limit($num)
			->onlyActual();

		return $this->em->getRepository(BuildingPlan::class)->fetch($query);
	}
	
	
	/**
	 * @param int
	 * @param User
	 * @param int
	 */
	public function setSavedGold($planId, User $user, $sum)
	{
		$plan = $this->getPlan($planId, TRUE);
		$info = $this->getInformationsForUser($user, $plan);
		$info->savedGold = $sum;
		$this->em->flush();
	}
	
	
	/**
	 * @param BuildingPlan
	 * @return int
	 */
	public function getSavedGoldSum(BuildingPlan $plan)
	{
		return (int) $this->em->createQuery("
			SELECT SUM(ui.savedGold)
			FROM App\Model\Entities\BuildingPlanUserInformations ui
			WHERE ui.plan = :plan
		")
			->setParameter("plan", $plan)
			->getSingleScalarResult();
	}
	
	
	/**
	 * @param User
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function deletePlan(User $user, $planId)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš smazat plán stavby");
		}
		
		$plan = $this->getPlan($planId, TRUE);
		$this->em->remove($plan);
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param int
	 * @param string
	 * @param int
	 * @param int
	 * @param string|\DateTime
	 * @param string|\DateTime
	 * @param string
	 * @throws Nette\InvalidArgumentException
	 */
	public function editPlan(User $user, $id, $buildingKey, $level, $discount, $from, $to, $comment)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš upravit plán stavby");
		}
		
		$plan = $this->getPlan($id, TRUE);
		if ($plan->building->key !== $buildingKey) {
			$building = new Building();
			$building->key = $buildingKey;
			$building->level = $level;
			$building->percentualDiscount = $discount;
			$this->em->persist($building);
			$plan->building = $building;
		} else {
			$plan->building->key = $buildingKey;
			$plan->building->level = $level;
			$plan->building->percentualDiscount = $discount;
		}
		
		$plan->from = is_object($from) ? $from : new Nette\Utils\DateTime($from);
		$plan->to = is_object($to) ? $to : new Nette\Utils\DateTime($to);
		$plan->comment = $comment;
		
		$this->em->flush();
	}
	
	
	/**
	 * @param int
	 * @param User
	 * @param int
	 * @param int
	 */
	public function setUsersDonationInfo($planId, User $user, $donationUserId, $amount)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš přidat informaci o příspěvku");
		}
		
		$donationUser = $this->userFacade->getUser($donationUserId, TRUE);
		$this->setDonationInfo($planId, $donationUser, $amount);
	}
	
	
	/**
	 * @param int
	 * @param User
	 * @param int
	 */
	public function setDonationInfo($planId, User $user, $amount)
	{
		$plan = $this->getPlan($planId, TRUE);
		$donation = $this->getUsersDonationForPlan($user, $plan);
		
		if ($donation !== NULL) {
			if ((int) $amount > 0) {
				$donation->amount = $amount;
			} else {
				$this->em->remove($donation);
			}
		} else {
			$donation = new BuildingPlanDonation();
			$donation->user = $user;
			$donation->amount = $amount;
			$plan->addDonation($donation);
		}
		
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param BuildingPlan
	 * @return BuildingPlanDonation|NULL
	 */
	public function getUsersDonationForPlan(User $user, BuildingPlan $plan)
	{
		return $this->em->getRepository(BuildingPlanDonation::class)->findOneBy(array(
			"user" => $user,
			"plan" => $plan
		));
	}
	

	/**
	 * @param BuildingPlan
	 * @param bool
	 * @return ResultSet
	 */
	public function getUsersInformations(BuildingPlan $plan, $onlyGuildMembers = TRUE)
	{
		$query = new BuildingPlanUsersInformationsQuery($plan);
		if ($onlyGuildMembers) {
			$query->onlyGuild();
		}
		
		return $this->em->getRepository(BuildingPlanUserInformations::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param BuildingPlan
	 * @return BuildingPlanUserInformations
	 */
	public function getInformationsForUser(User $user, BuildingPlan $plan)
	{
		return $this->em->getRepository(BuildingPlanUserInformations::class)->findOneBy(array(
			"user" => $user->id,
			"plan" => $plan->id
		));
	}
	
	
	/**
	 * @param User
	 * @param BuildingPlan
	 */
	public function addVisit(User $user, BuildingPlan $plan)
	{
		if ($user->isRoot()) {
			return;
		}
		
		$visit = new BuildingPlanVisit();
		$visit->user = $user;
		$visit->plan = $plan;
		$visit->date = new Nette\Utils\DateTime();
		
		$this->em->persist($visit);
		$this->em->flush();
	}
	
	
	/**
	 * @param array
	 * @return ResultSet
	 */
	public function getVisits(array $params = array())
	{
		$params = (object) $params;
		$query = new BuildingPlanVisitsQuery();
		
		if (isset($params->plan)) {
			$query->byPlan($params->plan);
		}
		if (isset($params->orderByDate)) {
			$query->orderByDate($params->orderByDate);
		}
		if (isset($params->distinctUsers)) {
			$query->distinctUsers();
		}
		
		return $this->em->getRepository(BuildingPlanVisit::class)->fetch($query);
	}
	
}