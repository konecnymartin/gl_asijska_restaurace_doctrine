<?php

namespace App\Model\Facades;

use Nette,
	Kdyby\Doctrine\EntityManager,
	Kdyby\Doctrine\ResultSet,
	App\Model\Entities\User,
	App\Model\Entities\Notification,
	App\Model\Entities\UserHiddenNotification,
	App\Model\Queries\NotificationsListQuery;


class NotificationFacade
{

	/** @var EntityManager */
	private $em;
	
	
	/**
	 * @param EntityManager
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	
	/**
	 * @param int|NULL
	 * @param bool
	 * @return Notification|NULL
	 * @throws Nette\InvalidArgumentException
	 */
	public function getNotification($id, $throwException = FALSE)
	{
		$notification = NULL;
		if ($id !== NULL) {
			$notification = $this->em->find(Notification::class, $id);
		}
		
		if ($notification === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takové oznámení neexistuje");
		}
		
		return $notification;
	}
	
	
	/**
	 * @return ResultSet
	 */
	public function getNotifications()
	{
		$query = new NotificationsListQuery();
		$query->orderByDate("DESC");
		
		return $this->em->getRepository(Notification::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @return ResultSet
	 */
	public function getUsersNotifications(User $user)
	{
		$query = new NotificationsListQuery();
		$query->byUser($user)
			->orderByDate("DESC");
		
		return $this->em->getRepository(Notification::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param string
	 * @throws Nette\InvalidArgumentException
	 */
	public function addNotification(User $user, $content)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš přidávat oznámení");
		}
		
		$notification = new Notification();
		$notification->content = $content;
		$notification->date = new Nette\Utils\DateTime();
		$notification->user = $user;
		
		$this->em->persist($notification);
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function deleteNotification(User $user, $id)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš smazat oznámení");
		}
		
		$notification = $this->getNotification($id, TRUE);
		$this->em->remove($notification);
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param int
	 */
	public function hideNotification(User $user, $id)
	{
		$notification = $this->getNotification($id, TRUE);
		$hidden = new UserHiddenNotification();
		$hidden->user = $user;
		$hidden->notification = $notification;
		
		$this->em->persist($hidden);
		$this->em->flush();
	}
	
}