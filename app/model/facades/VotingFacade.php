<?php

namespace App\Model\Facades;

use Nette,
	Kdyby\Doctrine\EntityManager,
	Kdyby\Doctrine\ResultSet,
	App\Model\Entities\User,
	App\Model\Entities\VotingTheme,
	App\Model\Entities\VotingAnswer,
	App\Model\Entities\VotingComment,
	App\Model\Entities\VotingVote,
	App\Model\Queries\VotingThemesListQuery;


class VotingFacade
{

	/** @var EntityManager */
	private $em;
	
	
	/**
	 * @param EntityManager
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	
	/**
	 * @param int|NULL
	 * @param bool
	 * @return VotingTheme|NULL
	 * @throws Nette\InvalidArgumentException
	 */
	public function getTheme($id, $throwException = FALSE)
	{
		$theme = NULL;
		if ($id !== NULL) {
			$theme = $this->em->find(VotingTheme::class, (int) $id);
		}
		
		if ($theme === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takové hlasování neexistuje");
		}
		
		return $theme;
	}
	
	
	/**
	 * @return ResultSet
	 */
	public function getThemes()
	{
		$query = new VotingThemesListQuery();
		$query->orderByDate("DESC");

		return $this->em->getRepository(VotingTheme::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param string
	 * @param string
	 * @param string|\DateTime
	 * @param array
	 */
	public function createVoting(User $user, $title, $content, $dateEnd, $answers)
	{
		$theme = new VotingTheme();
		$theme->title = $title;
		$theme->content = $content;
		$theme->dateCreated = new Nette\Utils\DateTime();
		$theme->dateEnd = is_object($dateEnd) ? $dateEnd : new Nette\Utils\DateTime($dateEnd);
	
		foreach ($answers as $content) {
			$answer = new VotingAnswer();
			$answer->content = $content;
			$theme->addAnswer($answer);
		}
		
		$user->addVotingTheme($theme);
		$this->em->flush();
	}
	
	
	/**
	 * @param VotingTheme
	 * @return ResultSet
	 */
	public function getUsersVotes(VotingTheme $theme)
	{
		return $this->em->createQuery("
			SELECT v
			FROM App\Model\Entities\VotingVote v
			INNER JOIN v.answer a
			WHERE a.theme = :theme
			ORDER BY a.content
		")
			->setParameter("theme", $theme)
			->getResult();
	}
	
	
	/**
	 * @param User
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function deleteTheme(User $user, $themeId)
	{
		$theme = $this->getTheme($themeId, TRUE);
		if (!$user->isAdmin() && $theme->user !== $user) {
			throw new Nette\InvalidArgumentException("Nemůžeš smazat toto hlasování");
		}
		
		$this->em->remove($theme);
		$this->em->flush();
	}
	
	
	/**
	 * @param int
	 * @param User
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function addVote($themeId, User $user, $answerId)
	{
		$theme = $this->getTheme($themeId, TRUE);
		if ($theme->expired()) {
			throw new Nette\InvalidArgumentException("Hlasování již skončilo");
		}
		
		$answer = $theme->getAnswer($answerId);
		if ($answer === NULL) {
			throw new Nette\InvalidArgumentException("Tato odpověď neexistuje");
		}
		if ($theme->userVoted($user)) {
			throw new Nette\InvalidArgumentException("Již jsi hlasoval");
		}
		
		$vote = new VotingVote();
		$vote->user = $user;
		$answer->addVote($vote);
		
		$this->em->flush();
	}
	
	
	/**
	 * @param int
	 * @param User
	 * @param string
	 */
	public function addComment($themeId, User $user, $content)
	{
		$theme = $this->getTheme($themeId, TRUE);
		if ($theme->expired()) {
			throw new Nette\InvalidArgumentException("Hlasování již skončilo, nelze přidávat komentáře");
		}
		
		$comment = new VotingComment();
		$comment->content = $content;
		$comment->date = new Nette\Utils\DateTime();
		$comment->user = $user;
		$theme->addComment($comment);
		
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @return ResultSet
	 */
	public function getUsersNotVotedThemes(User $user)
	{
		$query = new VotingThemesListQuery();
		$query->orderByDate("DESC")
			->userNotVotedIn($user);

		return $this->em->getRepository(VotingTheme::class)->fetch($query);
	}
	
}