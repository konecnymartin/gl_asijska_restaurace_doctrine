<?php

namespace App\Model\Facades;

use Nette,
	Kdyby\Doctrine\EntityManager,
	Kdyby\Doctrine\ResultSet,
	App\Model\Entities\User,
	App\Model\Entities\Absence,
	App\Model\Queries\AbsencesListQuery;


class AbsenceFacade
{

	/** @var EntityManager */
	private $em;
	
	
	/**
	 * @param EntityManager
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	
	/**
	 * @param int|NULL
	 * @param bool
	 * @return Absence|NULL
	 * @throws Nette\InvalidArgumentException
	 */
	public function getAbsence($id, $throwException = FALSE)
	{
		$absence = NULL;
		if ($id !== NULL) {
			$absence = $this->em->find(Absence::class, $id);
		}
		
		if ($absence === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Taková absence neexistuje");
		}
		
		return $absence;
	}
	
	
	/**
	 * @param array
	 * @return ResultSet
	 */
	public function getUsersAbsences(array $without)
	{
		$query = new AbsencesListQuery();
		$query->withoutUsers($without);

		return $this->em->getRepository(Absence::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param string|\DateTime
	 * @param string|\DateTime
	 * @param string
	 */
	public function addAbsence(User $user, $from, $to, $reason)
	{
		$absence = new Absence();
		$absence->from = is_object($from) ? $from : new Nette\Utils\DateTime($from);
		$absence->to = is_object($to) ? $to : new Nette\Utils\DateTime($to);
		$absence->reason = $reason;
		$user->addAbsence($absence);
		
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function deleteAbsence(User $user, $id)
	{
		$absence = $this->getAbsence($id, TRUE);
		if ($absence->user !== $user && !$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš smazat tuto absenci");
		}
		
		$this->em->remove($absence);
		$this->em->flush();
	}
	
}