<?php

namespace App\Model\Facades;

use Nette,
	Kdyby\Doctrine\EntityManager,
	Nette\Utils\Strings,
	Kdyby\Doctrine\ResultSet,
	App\Model\Entities\User,
	App\Model\Entities\UserLogin,
	App\Model\Entities\UserDescription,
	App\Model\Entities\ViewPermissions,
	App\Model\Entities\UserSettings,
	App\Model\Queries\UsersListQuery;


class UserFacade implements \Nette\Security\IAuthenticator
{

	/** @var EntityManager */
	private $em;
	
	
	/**
	 * @param EntityManager
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}
	
	
	/**
	 * @param int|NULL
	 * @param bool
	 * @return User|NULL
	 * @throws Nette\InvalidArgumentException
	 */
	public function getUser($id, $throwException = FALSE)
	{
		$user = NULL;
		if ($id !== NULL) {
			$user = $this->em->find(User::class, $id);
		}
		
		if ($user === NULL && $throwException) {
			throw new Nette\InvalidArgumentException("Takový uživatel neexistuje");
		}
		
		return $user;
	}
	
	
	/**
	 * @param string
	 * @return User|NULL
	 */
	public function getUserByName($name)
	{
		return $this->em->createQuery("
			SELECT u
			FROM App\Model\Entities\User u
			WHERE u.name LIKE :name
		")
			->setParameter("name", $name)
			->getOneOrNullResult();
	}
	
	
	/**
	 * @param User
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function addUser(User $user, $values)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš upravit uživatele");
		}
		
		$user = new User();
		$user->name = $values->name;
		$user->salt = Strings::random();
		$user->password = $this->hashPassword($values->password, $user->salt);
		$user->role = $values->role;
		$user->gladiatusId = !empty($values->gladiatusId) ? (int) $values->gladiatusId : NULL;
		$user->hidden = $values->hidden;
		$user->inGuild = $values->inGuild;
		
		$description = new UserDescription();
		$user->description = $description;

		$vp = (object) array_flip($values->viewPermissions);
		$viewPermissions = new ViewPermissions();
		$viewPermissions->rules = isset($vp->rules);
		$viewPermissions->aboutMembers = isset($vp->aboutMembers);
		$viewPermissions->hints = isset($vp->hints);
		$viewPermissions->buildingPlans = isset($vp->buildingPlans);
		$viewPermissions->votings = isset($vp->votings);
		$viewPermissions->itemOffers = isset($vp->itemOffers);
		$viewPermissions->absences = isset($vp->absences);
		$viewPermissions->contact = isset($vp->contact);
		$viewPermissions->notifications = isset($vp->notifications);
		
		$user->viewPermissions = $viewPermissions;
		$user->settings = new UserSettings();
		
		$this->em->persist($user);
		$this->em->flush();
	}
	
	
	/**
	 * @throws Nette\Security\AuthenticationException
	 * @return Nette\Security\Identity
	 */
	public function authenticate(array $credentials)
	{
		list($name, $password) = $credentials;

		$user = $this->getUserByName($name);

		if ($user === NULL) {
			throw new \Nette\Security\AuthenticationException("Bylo zadáno špatné jméno");
		}
		if ($user->password !== $this->hashPassword($password, $user->salt)) {
			throw new \Nette\Security\AuthenticationException("Bylo zadáno špatné heslo");
		}

		return new \Nette\Security\Identity($user->id);
	}
	
	
	/**
	 * @param string
	 * @param string
	 * @return string
	 */
	private function hashPassword($pass, $salt)
	{
		return hash_hmac("SHA512", $pass, $salt);
	}

	
	/**
	 * @param array
	 * @return ResultSet
	 */
	public function getUsersList($flags = array())
	{
		// when flipping array, the first key is zero
		// and because of that condition   if ($flags->flag) wouldn't work
		// so it's neccessary to increment +1 to all keys
		array_unshift($flags, NULL);
		unset($flags[0]);
		$flags = (object) array_flip($flags);
		
		$query = new UsersListQuery();
		$query->orderByName();

		if (!empty($flags)) {
			if (isset($flags->withDescription)) {
				$query->withDescription();
			}
		}
	
		return $this->em->getRepository(User::class)->fetch($query);
	}
	
	
	/**
	 * @return ResultSet
	 */
	public function getVisibleUsersList()
	{
		$query = new UsersListQuery();
		$query->orderByName()
			->onlyVisible();
	
		return $this->em->getRepository(User::class)->fetch($query);
	}
	
	
	/**
	 * @return ResultSet
	 */
	public function getGuildUsersList()
	{
		$query = new UsersListQuery();
		$query->orderByName()
			->inGuild();
	
		return $this->em->getRepository(User::class)->fetch($query);
	}
	
	
	/**
	 * @param User
	 * @param Nette\ArrayHash
	 * @throws Nette\InvalidArgumentException
	 */
	public function editUser(User $user, $data)
	{
		if (!$user->isAdmin()) {
			throw new Nette\InvalidArgumentException("Nemůžeš upravit uživatele");
		}
		
		$userEdit = $this->getUser($data->userId);
		if ($userEdit->isRoot() && !$user->isRoot()) {
			throw new Nette\InvalidArgumentException("Nemůžeš upravit tohoto uživatele");
		}
		
		
		$userEdit->name = $data->name;
		$userEdit->gladiatusId = !empty($data->gladiatusId) ? $data->gladiatusId : 0;
		$userEdit->role = $data->role;
		$userEdit->hidden = $data->hidden;
		$userEdit->inGuild = $data->inGuild;
		$userEdit->description->content = !empty($data->description) ? $data->description : NULL;
		
		$vp = (object) array_flip($data->viewPermissions);
		$viewPermissions = $userEdit->viewPermissions;
		$viewPermissions->rules = isset($vp->rules);
		$viewPermissions->aboutMembers = isset($vp->aboutMembers);
		$viewPermissions->hints = isset($vp->hints);
		$viewPermissions->buildingPlans = isset($vp->buildingPlans);
		$viewPermissions->votings = isset($vp->votings);
		$viewPermissions->itemOffers = isset($vp->itemOffers);
		$viewPermissions->absences = isset($vp->absences);
		$viewPermissions->contact = isset($vp->contact);
		$viewPermissions->notifications = isset($vp->notifications);
		
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param string
	 */
	public function addLogin(User $user, $ip)
	{
		$login = new UserLogin();
		$login->ip = $ip;
		$login->date = new Nette\Utils\DateTime();
		$user->addLogin($login);
		
		$this->em->flush();
	}
	
	
	/**
	 * @param int
	 * @throws Nette\InvalidArgumentException
	 */
	public function deleteUser($id)
	{
		$user = $this->getUser($id);
		if ($user->isRoot()) {
			throw new Nette\InvalidArgumentException("Nelze odstranit tohoto uživatele");
		}
		
		$this->em->remove($user);
		
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param string
	 * @param string
	 * @throws Nette\InvalidArgumentException
	 */
	public function changePassword(User $user, $old, $new)
	{
		$salt = $user->salt;
		$oldHash = $this->hashPassword($old, $salt);
		if ($oldHash !== $user->password) {
			throw new Nette\InvalidArgumentException("Bylo zadáno špatné aktuální heslo");
		}
		
		$user->password = $this->hashPassword($new, $salt);
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param string|NULL
	 */
	public function changeEmail(User $user, $email)
	{
		if (empty($email)) {
			$email = NULL;
		}
		
		$user->email = $email;
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param string|NULL
	 */
	public function changeDescription(User $user, $content)
	{
		$user->description->content = $content;
		$this->em->flush();
	}
	
	
	/**
	 * @param User
	 * @param Nette\ArrayHash
	 */
	public function changeSettings(User $user, $data)
	{
		$settings = $user->settings;
		$settings->sendEmailWhenBuildingPlanCreated = $data->sendEmailWhenBuildingPlanCreated;
		$settings->levelPowerupActive = $data->levelPowerupActive;
		
		$this->em->flush();
	}
	
	
	/**
	 * Used by administration tool
	 *
	 * @param User
	 * @param int
	 * @param string
	 * @param string
	 * @throws Nette\InvalidArgumentException
	 */
	public function changePasswordAdmin(User $user, $userId, $password)
	{
		if (!$user->isAdmin()) {
			throw new Nette\Security\AuthenticationException("Nemáš právo změnit heslo");
		}
		
		$triggeringUser = $this->getUser($userId, TRUE);
		$triggeringUser->password = $this->hashPassword($password, $triggeringUser->salt);
		$this->em->flush();
	}
	
}