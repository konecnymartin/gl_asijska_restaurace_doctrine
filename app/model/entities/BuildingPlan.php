<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby,
	Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="building_plan")
 */
class BuildingPlan extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="date_from", type="datetime")
	 */
	protected $from;
	
	/**
	 * @ORM\Column(name="date_to", type="datetime")
	 */
	protected $to;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $comment;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $optional;
	
	/**
	 * @ORM\OneToOne(targetEntity="Building")
	 * @ORM\JoinColumn
	 */
	protected $building;
	
	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="buildingPlans")
	 */
	protected $user;
	
	/**
	 * @ORM\OneToMany(targetEntity="BuildingPlanLevelDonation", mappedBy="plan", cascade={"persist", "remove"})
	 */
	protected $levelDonations;
	
	/**
	 * @ORM\OneToMany(targetEntity="BuildingPlanDonation", mappedBy="plan", cascade={"persist", "remove"})
	 */
	protected $donations;
	
	/**
	 * @ORM\OneToMany(targetEntity="BuildingPlanUserInformations", mappedBy="plan", cascade={"persist", "remove"})
	 */
	protected $usersInformations;
	
	
	public function __construct()
	{
		$this->levelDonations = new ArrayCollection();
		$this->donations = new ArrayCollection();
		$this->usersInformations = new ArrayCollection();
	}
	
	
	/**
	 * @return bool
	 */
	public function expired()
	{
		$now = new Nette\Utils\DateTime();
		
		return $now > $this->to;
	}
	
	
	/**
	 * @return BuildingPlanLevelDonation
	 */
	public function getLevelDonations()
	{
		return $this->levelDonations;
	}
	
	
	/**
	 * @return BuildingPlanDonation
	 */
	public function getDonations()
	{
		return $this->donations;
	}
	
	
	/**
	 * @param BuildingPlanLevelDonation
	 */
	public function addLevelDonation(BuildingPlanLevelDonation $donation)
	{
		$this->levelDonations[] = $donation;
		$donation->plan = $this;
	}
	
	
	/**
	 * @param User
	 * @return BuildingPlanGoldSave
	 */
	public function getInformationsForUser(User $user)
	{
		foreach ($this->usersInformations as $info) {
			if ($info->user === $user) {
				return $info;
			}
		}
		
		return NULL;
	}
	
	
	/**
	 * @param User
	 * @return int
	 */
	public function calculateUsersDonation(User $user)
	{
		$donationPerLevel = $this->getUsersDonationPerLevel($user);
		$donation = $user->getLevel() * $donationPerLevel;
		
		return $donation;
	}
	
	
	/**
	 * @param User
	 * @return int
	 */
	public function getUsersDonationPerLevel(User $user)
	{
		foreach ($this->levelDonations as $donation) {
			if ($donation->levelFrom <= $user->level && $donation->levelTo >= $user->level) {
				return $donation->donation;
			}
		}
		
		return 0;
	}
	

	/**
	 * @param BuildingPlanDonation
	 */
	public function addDonation(BuildingPlanDonation $donation)
	{
		$this->donations[] = $donation;
		$donation->plan = $this;
	}
	
	
	/**
	 * @param BuildingPlanUserInformations
	 */
	public function addUserInformations(BuildingPlanUserInformations $info)
	{
		$this->usersInformations[] = $info;
		$info->plan = $this;
	}

}