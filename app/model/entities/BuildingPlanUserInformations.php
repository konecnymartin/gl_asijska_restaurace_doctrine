<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="building_plan_user_informations")
 */
class BuildingPlanUserInformations extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="donation_required", type="boolean")
	 */
	protected $donationRequired = TRUE;
	
	/**
	 * @ORM\Column(name="saved_gold", type="integer")
	 */
	protected $savedGold = 0;
	
	/**
	 * @ORM\ManyToOne(targetEntity="BuildingPlan", inversedBy="goldSaves")
	 */
	protected $plan;
	
	/**
	 * @ORM\OneToOne(targetEntity="User")
	 */
	protected $user;

}