<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="prefix_suffix")
 */
class PrefixSuffix extends Kdyby\Doctrine\Entities\BaseEntity
{

	const PREFIX = 1,
		SUFFIX = 2;


	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $type;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $damage;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $armour;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $strength;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $skill;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $agility;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $constitution;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $charisma;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $intelligence;
	
	/**
	 * @ORM\Column(name="strength_percentually", type="integer")
	 */
	protected $strengthPercentually;
	
	/**
	 * @ORM\Column(name="skill_percentually", type="integer")
	 */
	protected $skillPercentually;
	
	/**
	 * @ORM\Column(name="agility_percentually", type="integer")
	 */
	protected $agilityPercentually;
	
	/**
	 * @ORM\Column(name="constitution_percentually", type="integer")
	 */
	protected $constitutionPercentually;
	
	/**
	 * @ORM\Column(name="charisma_percentually", type="integer")
	 */
	protected $charismaPercentually;
	
	/**
	 * @ORM\Column(name="intelligence_percentually", type="integer")
	 */
	protected $intelligencePercentually;
	
	/**
	 * @ORM\Column(name="critical_strike", type="integer")
	 */
	protected $criticalStrike;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $blocking;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $resilience;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $threat;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $healing;
	
	/**
	 * @ORM\Column(name="critical_healing", type="integer")
	 */
	protected $criticalHealing;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $health;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $level;
	
}