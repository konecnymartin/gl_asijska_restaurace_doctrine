<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="building_level_donation")
 */
class BuildingPlanLevelDonation extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="level_from", type="integer")
	 */
	protected $levelFrom;
	
	/**
	 * @ORM\Column(name="level_to", type="integer")
	 */
	protected $levelTo;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $donation;
	
	/**
	 * @ORM\ManyToOne(targetEntity="BuildingPlan", inversedBy="levelDonations")
	 */
	protected $plan;

}