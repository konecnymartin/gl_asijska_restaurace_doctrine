<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="user_settings")
 */
class UserSettings extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="send_email_building_plan_create", type="boolean")
	 */
	protected $sendEmailWhenBuildingPlanCreated = FALSE;
	
	/**
	 * when user has an in-game powerup he can see +2 levels distance with
	 * @ORM\Column(name="level_powerup_active", type="boolean")
	 */
	protected $levelPowerupActive = FALSE;

}