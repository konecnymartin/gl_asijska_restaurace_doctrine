<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 */
class Building extends Kdyby\Doctrine\Entities\BaseEntity
{

	// gladiatus keys
	const FORUM = "forum_gladiatorius",
		BANK = "save",
		TRAINING_GROUND = "training_camp",
		MARKET = "guild_market",
		LIBRARY = "library",
		BATHHOUSE = "bathhouse",
		NEGOTIUM_X = "jailhouse",
		STORAGE = "storage",
		WAR_MASTER_HALL = "war_master",
		TEMPLUM = "temple",
		VILLA_MEDICI = "doctor";


	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="`key`", type="string")
	 */
	protected $key;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $level;
	
	/**
	 * @ORM\Column(name="percentual_discount", type="integer")
	 */
	protected $percentualDiscount = 0;

	
	
	/**
	 * @return array
	 */
	public static function getTypes()
	{
		return array(
			Building::FORUM, Building::BANK, Building::TRAINING_GROUND,
			Building::MARKET, Building::LIBRARY, Building::BATHHOUSE,
			Building::NEGOTIUM_X, Building::STORAGE, Building::WAR_MASTER_HALL,
			Building::TEMPLUM, Building::VILLA_MEDICI 
		);
	}
	
	
	/**
	 * @return array
	 */
	public static function getPriceCoefficients()
	{
		return array(
			Building::FORUM => array(1.2, 6.5),
			Building::BANK => array(4.8, 4.5),
			Building::TRAINING_GROUND => array(3.9, 4.5),
			Building::MARKET => array(2.7, 4.5),
			Building::LIBRARY => array(2.5, 4.5),
			Building::BATHHOUSE => array(3.3, 4.5),
			Building::NEGOTIUM_X => array(4.1, 4.5),
			Building::STORAGE => array(9, 4.5),
			Building::WAR_MASTER_HALL => array(2.3, 4.5),
			Building::TEMPLUM => array(2, 4.5),
			Building::VILLA_MEDICI => array(4.1, 4.5)
		);
	}
	
	
	/**
	 * @return int
	 */
	public function getLevel()
	{
		/*
		if ($this->level === NULL) {
			$request = new CurlRequest("http://s23.cz.gladiatus.gameforge.com/game/index.php?mod=guild&i=599");
			$html = $request->get()->response;
			
			// need to find the exact element with the building name and level
			$patternDiv = "\<div class=\"map_label\" style=\"(.*)\"\>(.*)\<\/div\>";
			$patternLink = "\<a href=\"(.*)\"\ class=\"map_label\" style=\"(.*)\"\>(.*)\<\/a\>";
			preg_match_all("#{$patternDiv}|{$patternLink}#", $html, $matches);
			
			$buildings = array();
			$names = $matches[2];
			unset($names[0]);  // first element is empty
			$mainHall = $matches[5][0];  // because of link, not div
			$buildings = array_merge(array($mainHall), $names);
			
			// buildings are now in format like:   Forum Gladiatorius (7)
			// so it's needed to find the number
			preg_match("#[0-9]#", $buildings[$this->type], $matchedNumber);
			
			$this->level = (int) $matchedNumber[0];
		}
		*/
		
		return $this->level;
	}
	
	
	/**
	 * @return int
	 */
	public function getDefaultPrice()
	{
		$coefs = self::getPriceCoefficients()[$this->key];
		$price = floor(pow($coefs[0] * $this->level, $coefs[1]));
		
		return $price;
	}
	
	
	/**
	 * @return \stdClass
	 */
	public function getPrice()
	{
		$default = $this->getDefaultPrice();
		$price = floor($default - ($default / 100 * $this->percentualDiscount));
		
		return $price;
	}

}