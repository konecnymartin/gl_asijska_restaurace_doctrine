<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 */
class Absence extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $reason;
	
	/**
	 * @ORM\Column(name="`from`", type="datetime")
	 */
	protected $from;
	
	/**
	 * @ORM\Column(name="`to`", type="datetime")
	 */
	protected $to;
	
	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="absences")
	 */
	protected $user;
	
	
	/**
	 * @return bool
	 */
	public function expired()
	{
		$now = new Nette\Utils\DateTime();
		
		return $now > $this->to;
	}

}