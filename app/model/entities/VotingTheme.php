<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby,
	Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="voting_theme")
 */
class VotingTheme extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $title;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $content;
	
	/**
	 * @ORM\Column(name="date_created", type="datetime")
	 */
	protected $dateCreated;

	/**
	 * @ORM\Column(name="date_end", type="datetime")
	 */
	protected $dateEnd = NULL;
	
	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="votingThemes")
	 */
	protected $user;
	
	/**
	 * @ORM\OneToMany(targetEntity="VotingAnswer", mappedBy="theme", cascade={"persist", "remove"})
	 */
	protected $answers;
	
	/**
	 * @ORM\OneToMany(targetEntity="VotingComment", mappedBy="theme", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"date" = "DESC"})
	 */
	protected $comments;
	
	
	public function __construct()
	{
		$this->answers = new ArrayCollection();
		$this->comments = new ArrayCollection();
	}
	
	
	/**
	 * @param VotingAnswer
	 */
	public function addAnswer(VotingAnswer $answer)
	{
		$this->answers[] = $answer;
		$answer->theme = $this;
	}
	
	
	/**
	 * @return ArrayCollection
	 */
	public function getAnswers()
	{
		return $this->answers;
	}
	
	
	/**
	 * @param int
	 * @return VotingAnswer|NULL
	 */
	public function getAnswer($id)
	{
		foreach ($this->answers as $answer) {
			if ($answer->id === $id) {
				return $answer;
			}
		}
		
		return NULL;
	}
	
	
	/**
	 * @param User
	 * @return bool
	 */
	public function userVoted(User $user)
	{
		foreach ($this->answers as $answer) {
			foreach ($answer->votes as $vote) {
				if ($vote->user === $user) {
					return TRUE;
				}
			}
		}
		
		return FALSE;
	}
	
	
	/**
	 * @return bool
	 */
	public function expired()
	{
		$now = new Nette\Utils\DateTime();
		
		return $now > $this->dateEnd;
	}
	
	
	/**
	 * @return ArrayCollection
	 */
	public function getComments()
	{
		return $this->comments;
	}
	
	
	/**
	 * @param VotingComment
	 */
	public function addComment(VotingComment $comment)
	{
		$this->comments[] = $comment;
		$comment->theme = $this;
	}

}