<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="item_offer")
 */
class ItemOffer extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="item_quality", type="integer")
	 */
	protected $itemQuality;
	
	/**
	 * @ORM\Column(name="item_min_damage", type="integer")
	 */
	protected $itemMinDamage = NULL;
	
	/**
	 * @ORM\Column(name="item_max_damage", type="integer")
	 */
	protected $itemMaxDamage = NULL;
	
	/**
	 * @ORM\Column(name="item_armour", type="integer")
	 */
	protected $itemArmour = NULL;
	
	/**
	 * @ORM\Column(name="item_improvement_type", type="integer")
	 */
	protected $itemImprovementType = NULL;
	
	/**
	 * @ORM\Column(name="item_improvement_value", type="integer")
	 */
	protected $itemImprovementValue = NULL;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $price;
	
	/**
	 * @ORM\Column(name="date_added", type="datetime")
	 */
	protected $dateAdded;
	
	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="itemOffers")
	 */
	protected $user;
	
	/**
	 * @ORM\OneToOne(targetEntity="Item", fetch="EAGER")
	 */
	protected $item;
	
	/**
	 * @ORM\OneToOne(targetEntity="PrefixSuffix", fetch="EAGER")
	 * @ORM\JoinColumn(name="prefix_id", referencedColumnName="id")
	 */
	protected $prefix;
	
	/**
	 * @ORM\OneToOne(targetEntity="PrefixSuffix", fetch="EAGER")
	 * @ORM\JoinColumn(name="suffix_id", referencedColumnName="id")
	 */
	protected $suffix;
	
	/**
	 * @var \stdClass
	 */
	protected $itemAttributes = NULL;
	
	
	/**
	 * @return \stdClass
	 */
	public function getItemAttributes()
	{
		if ($this->itemAttributes !== NULL) {
			return $this->itemAttributes;
		}
		
		$item = $this->item;
		$this->itemAttributes = $attrs = (object) array(
			"minDamage" => $item->minDamage,
			"maxDamage" => $item->maxDamage,
			"damage" => 0,
			"armour" => $item->armour,
			"strength" => 0,
			"skill" => 0,
			"agility" => 0,
			"constitution" => 0,
			"charisma" => 0,
			"intelligence" => 0,
			"strengthPercentually" => 0,
			"skillPercentually" => 0,
			"agilityPercentually" => 0,
			"constitutionPercentually" => 0,
			"charismaPercentually" => 0,
			"intelligencePercentually" => 0,
			"criticalStrike" => 0,
			"blocking" => 0,
			"resilience" => 0,
			"threat" => 0,
			"healing" => 0,
			"criticalHealing" => 0,
			"health" => 0,
			"level" => $item->level
		);

		$this->addAttributes($this->prefix);
		$this->addAttributes($this->suffix);
		$this->excludeAttributesByItemType();
		
		$multipleFactor = Item::getQualityMultipleFactor($this->itemQuality);
		$skip = array("minDamage", "maxDamage", "level");
		if ($multipleFactor != 1) {
			foreach ($attrs as $key => &$val) {
				if (in_array($key, $skip)) {
					continue;
				}
				
				if (preg_match("#Percentually$#", $key)) {
					$val = round($val * $multipleFactor);
				} else {
					if ($val > 0) {
						$val = floor($val * $multipleFactor);
					} else {
						$val = ceil($val * $multipleFactor);
					}
				}	
			}
		}
		
		return $attrs;
	}
	
	
	/**
	 * @param PrefixSuffix
	 */
	private function addAttributes($obj)
	{
		if ($obj !== NULL) {
			foreach ($this->itemAttributes as $key => &$value) {
				if (!isset($obj->$key)) {  // e.g. min_damage, max_damage
					continue;
				}
				
				$value += $obj->$key;
			}
		}
	}
	
	
	/**
	 * @return string
	 */
	public function getFullItemName()
	{
		$name = $this->item->name;
		if ($this->prefix !== NULL) {
			$name = $this->prefix->name . " " . $name;
		}
		if ($this->suffix !== NULL) {
			$name .= " " . $this->suffix->name;
		}
		
		return $name;
	}
	
	

	public function excludeAttributesByItemType()
	{
		$attrs = $this->itemAttributes;
		switch ($this->item->type) {
			case Item::TYPE_ARMOR:
				$attrs->agilityPercentually = 0;
				break;
			
			case Item::TYPE_SHIELD:
				$attrs->charismaPercentually = 0;
				break;
			
			case Item::TYPE_RING:
			case Item::TYPE_AMULET:
				$attrs->strengthPercentually = 0;
				break;
			
			case Item::TYPE_SHOES:
				$attrs->skillPercentually = 0;
				break;
		}
	}
	
	
	/**
	 * @param User
	 * @return bool
	 */
	public function canUserSeeItemOnMarket(User $user)
	{
		$userLevel =  $user->getGladiatusData()->level;
		
		// for users who don't have their gladiatus ID set
		if ($userLevel === 0) {
			return TRUE;
		}
		
		$maxLevel = $userLevel * 1.25;
		
		if ($maxLevel > ($userLevel + 9)) {
			$maxLevel = $userLevel + 9;
		}
		if ($user->settings->levelPowerupActive) {
			$maxLevel += 2;
		}
		
		return $maxLevel >= $this->getItemAttributes()->level;
	}

}