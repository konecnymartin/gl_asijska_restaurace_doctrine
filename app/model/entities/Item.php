<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby;


/**
 * @ORM\Entity
 */
class Item extends Kdyby\Doctrine\Entities\BaseEntity
{

	const QUALITY_BASIC = 1,
		QUALITY_GREEN = 2,
		QUALITY_BLUE = 3,
		QUALITY_PINK = 4,
		QUALITY_ORANGE = 5,
		QUALITY_RED = 6;
		
	const IMPROVEMENT_NONE = 0,
		IMPROVEMENT_GRINDSTONE = 1,
		IMPROVEMENT_PROTECTIVE_GEAR = 2,
		IMPROVEMENT_BLUE_POWDER = 3,
		IMPROVEMENT_YELLOW_POWDER = 4,
		IMPROVEMENT_GREEN_POWDER = 5,
		IMPROVEMENT_ORANGE_POWDER = 6,
		IMPROVEMENT_PURPLE_POWDER = 7,
		IMPROVEMENT_RED_POWDER = 8;
		
	const TYPE_WEAPON = 1,
		TYPE_SHIELD = 2,
		TYPE_ARMOR = 3,
		TYPE_HELMET = 4,
		TYPE_GLOVE = 5,
		TYPE_RING = 6,
		TYPE_SHOES = 8,
		TYPE_AMULET = 9;


	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(name="min_damage", type="integer")
	 */
	protected $minDamage;
	
	/**
	 * @ORM\Column(name="max_damage", type="integer")
	 */
	protected $maxDamage;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $armour;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $level;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $type;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $num;
	
	
	/**
	 * @param int
	 * @return int|float
	 */
	public static function getQualityMultipleFactor($quality)
	{
		static $factors = array(
			self::QUALITY_BASIC => 1,
			self::QUALITY_GREEN => 1,
			self::QUALITY_BLUE => 1.15,
			self::QUALITY_PINK => 1.3,
			self::QUALITY_ORANGE => 1.5,
			self::QUALITY_RED => 1.75
		);
		
		return $factors[$quality];
	}
	
}