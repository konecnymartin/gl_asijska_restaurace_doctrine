<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="building_donation")
 */
class BuildingPlanDonation extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	protected $amount;
	
	/**
	 * @ORM\OneToOne(targetEntity="User")
	 * @ORM\JoinColumn
	 */
	protected $user;
	
	/**
	 * @ORM\ManyToOne(targetEntity="BuildingPlan", inversedBy="donations")
	 */
	protected $plan;

}