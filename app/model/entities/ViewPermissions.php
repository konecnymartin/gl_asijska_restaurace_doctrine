<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="user_view_permission")
 */
class ViewPermissions extends Kdyby\Doctrine\Entities\BaseEntity
{

	const RULES = "rules",
		ABOUT_MEMBERS = "aboutMembers",
		HINTS = "hints",
		BUILDING_PLANS = "buildingPlans",
		VOTINGS = "votings",
		ITEM_OFFERS = "itemOffers",
		ABSENCES = "absences",
		CONTACT = "contact",
		NOTIFICATIONS = "notifications";

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $rules = FALSE;
	
	/**
	 * @ORM\Column(name="about_members", type="boolean")
	 */
	protected $aboutMembers = FALSE;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $hints = FALSE;
	
	/**
	 * @ORM\Column(name="building_plans", type="boolean")
	 */
	protected $buildingPlans = FALSE;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $votings = FALSE;
	
	/**
	 * @ORM\Column(name="item_offers", type="boolean")
	 */
	protected $itemOffers = FALSE;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $absences = FALSE;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $contact = FALSE;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $notifications = FALSE;
	
	
	/**
	 * @return \stdClass
	 */
	public static function getKeys()
	{
		return (object) array(
			self::RULES => NULL,
			self::ABOUT_MEMBERS => NULL,
			self::HINTS => NULL,
			self::BUILDING_PLANS => NULL,
			self::VOTINGS => NULL,
			self::ITEM_OFFERS => NULL,
			self::ABSENCES => NULL,
			self::CONTACT => NULL,
			self::NOTIFICATIONS => NULL
		);
	}

}