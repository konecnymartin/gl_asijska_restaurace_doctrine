<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="user_hidden_notification")
 */
class UserHiddenNotification extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\OneToOne(targetEntity="User", fetch="EAGER", cascade={"persist"})
	 */
	protected $user;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Notification", inversedBy="Notification", cascade={"persist"})
	 */
	protected $notification;

}