<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby;


/**
 * @ORM\Entity
 */
class Notification extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $content;
	
	/**
	 * @ORM\Column(type="datetime")
	 */
	protected $date;
	
	/**
	 * @ORM\OneToOne(targetEntity="User", fetch="EAGER", cascade={"persist"})
	 */
	protected $user;
	
	/**
	 * @ORM\OneToMany(targetEntity="UserHiddenNotification", mappedBy="notification", fetch="LAZY")
	 */
	protected $userHiddenNotifications;

}