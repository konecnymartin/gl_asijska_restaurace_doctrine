<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby,
	Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity
 * @ORM\Table(name="voting_answer")
 */
class VotingAnswer extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @ORM\Column(type="text")
	 */
	protected $content;
	
	/**
	 * @ORM\OneToMany(targetEntity="VotingVote", mappedBy="answer", cascade={"persist", "remove"})
	 */
	protected $votes;
	
	/**
	 * @ORM\ManyToOne(targetEntity="VotingTheme", inversedBy="answers")
	 */
	protected $theme;
	
	
	public function __construct()
	{
		$this->votes = new ArrayCollection();
	}
	
	
	/**
	 * @return ArrayCollection
	 */
	public function getVotes()
	{
		return $this->votes;
	}
	
	
	/**
	 * @param VotingVote
	 */
	public function addVote(VotingVote $vote)
	{
		$this->votes[] = $vote;
		$vote->answer = $this;
	}
	
	
	/**
	 * @param User
	 * @return bool
	 */
	public function userVoted(User $user)
	{
		foreach ($this->votes as $vote) {
			if ($vote->user === $user) {
				return TRUE;
			}
		}
		
		return FALSE;
	}

}