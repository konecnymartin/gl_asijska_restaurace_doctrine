<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Kdyby,
	Doctrine\Common\Collections\ArrayCollection,
	Kdyby\Curl\Request as CurlRequest,
	Nette;


/**
 * @ORM\Entity
 */
class User extends Kdyby\Doctrine\Entities\BaseEntity
{

	const ROLE_USER = "user",
		ROLE_ADMIN = "admin",
		ROLE_ROOT = "root";


	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $password;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $email = NULL;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $role;
	
	/**
	 * @ORM\Column(name="gladiatus_id", type="integer")
	 */
	protected $gladiatusId = NULL;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $hidden = FALSE;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $salt;
	
	/**
	 * @ORM\Column(name="in_guild", type="boolean")
	 */
	protected $inGuild = TRUE;
	
	/**
	 * @ORM\OneToOne(targetEntity="UserDescription", inversedBy="user", cascade={"persist", "remove"})
	 */
	protected $description;
	
	/**
	 * @ORM\OneToMany(targetEntity="UserLogin", mappedBy="user", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"date" = "DESC"})
	 */
	protected $logins;
	
	/**
	 * @ORM\OneToMany(targetEntity="Absence", mappedBy="user", cascade={"persist", "remove"})
	 * @ORM\OrderBy({"from" = "DESC"})
	 */
	protected $absences;
	
	/**
	 * @ORM\OneToMany(targetEntity="BuildingPlan", mappedBy="user")
	 */
	protected $buildingPlans;
	
	/**
	 * @ORM\OneToMany(targetEntity="VotingTheme", mappedBy="user", cascade={"persist"})
	 */
	protected $votingThemes;
	
	/**
	 * @ORM\OneToOne(targetEntity="UserSettings", cascade={"persist", "remove"}, fetch="EAGER")
	 */
	protected $settings;
	
	/**
	 * @ORM\OneToMany(targetEntity="ItemOffer", mappedBy="user", cascade={"persist", "remove"})
	 */
	protected $itemOffers;
	
	/**
	 * @ORM\OneToOne(targetEntity="ViewPermissions", cascade={"persist", "remove"}, fetch="EAGER")
	 * @ORM\JoinColumn(name="view_permission_id", referencedColumnName="id")
	 */
	protected $viewPermissions;
	
	/** @var \stdClass|NULL */
	protected $gladiatusData = NULL;
	
	
	public function __construct()
	{
		$this->logins = new ArrayCollection();
		$this->absences = new ArrayCollection();
		$this->buildingPlans = new ArrayCollection();
		$this->votingThemes = new ArrayCollection();
		$this->itemOffers = new ArrayCollection();
	}
	
	
	/**
	 * @return bool
	 */
	public function isAdmin()
	{
		return $this->role === self::ROLE_ADMIN || $this->role === self::ROLE_ROOT;
	}
	
	
	/**
	 * @return bool
	 */
	public function isRoot()
	{
		return $this->role === self::ROLE_ROOT;
	}
	
	
	/**
	 * @return bool
	 */
	public function isHidden()
	{
		return $this->hidden;
	}
	
	
	/**
	 * @param UserLogin
	 */
	public function addLogin(UserLogin $login)
	{
		$this->logins[] = $login;
		$login->user = $this;
	}
	
	
	/**
	 * @return ArrayCollection
	 */
	public function getLogins()
	{
		return $this->logins;
	}
	
	
	/**
	 * @param Absence
	 */
	public function addAbsence(Absence $absence)
	{
		$this->absences[] = $absence;
		$absence->user = $this;
	}
	
	
	/**
	 * @return ArrayCollection
	 */
	public function getAbsences()
	{
		return $this->absences;
	}
	
	
	/**
	 * @return \stdClass|NULL
	 */
	public function getGladiatusData()
	{
		if ($this->gladiatusData !== NULL) {
			return $this->gladiatusData;
		}
		if ($this->gladiatusId === NULL || $this->gladiatusId === 0) {
			return (object) array(
				"strength" => 0,
				"skill" => 0,
				"agility" => 0,
				"constitution" => 0,
				"charisma" => 0,
				"intelligence" => 0,
				"level" => 0,
			);
		}
		
		$request = new CurlRequest("http://s23.cz.gladiatus.gameforge.com/game/index.php?mod=player&p={$this->gladiatusId}");
		$html = $request->get()->response;
		
		// attributes
		$names = array("strength", "skill", "agility", "constitution", "charisma", "intelligence");
		$data = array();

		preg_match_all('#<td(.*)>(.*)<\/td>#', $html, $matches);  // find all <td> tags
		$arr = &$matches[0];
		for ($i = 3; $i <= 8; $i++) {   // 3 - 8 are attributes
			$attrString = &$arr[$i];
			$attrString = strip_tags($attrString);  // remove all tags to get only the text
			
			preg_match("#Základní\:(\d)+#", $attrString, $matches);  // find all numbers next to the "Základní:" string
			$field = $matches[0];
			$attr = (int) str_replace("Základní:", "", $field);
			$data[$names[$i - 3]] = $attr;
		}
		
		// level
		// it's needed to find the element with level number
		preg_match("#\<span id=\"char_level\"(.*)\>([0-9]*)\<\/span\>#", $html, $matches);
		$data["level"] = isset($matches[2]) ? (int) $matches[2] : 0;
		
		$this->gladiatusData = (object) $data;
		
		return $this->gladiatusData;
	}
	
	
	/**
	 * @return int
	 */
	public function getLevel()
	{
		/*
		if ($this->level === NULL) {
			if ($this->gladiatusId === NULL) {
				$this->level = 0;
				return;
			}
		
			$request = new CurlRequest("http://s23.cz.gladiatus.gameforge.com/game/index.php?mod=player&p={$this->gladiatusId}");
			$html = $request->get()->response;
			
			// it's needed to find the element with level number
			preg_match("#\<span id=\"char_level\"(.*)\>([0-9]*)\<\/span\>#", $html, $matches);

			$level = isset($matches[2]) ? (int) $matches[2] : 0;
			$this->level = $level;
		}
		
		return $this->level;
		*/
		
		return $this->getGladiatusData()->level;
	}
	
	
	/**
	 * @param VotingTheme
	 */
	public function addVotingTheme(VotingTheme $theme)
	{
		$this->votingThemes[] = $theme;
		$theme->user = $this;
	}
	
	
	/**
	 * @return ArrayCollection
	 */
	public function getVotingThemes()
	{
		return $this->votingThemes;
	}

	
	/**
	 * @return ArrayCollection
	 */
	public function getItemOffers()
	{
		return $this->itemOffers;
	}
	
	
	/**
	 * @param ItemOffer
	 */
	public function addItemOffer(ItemOffer $offer)
	{
		$this->itemOffers[] = $offer;
		$offer->user = $this;
	}
	
	
	/**
	 * @param int
	 * @return ItemOffer|NULL
	 */
	public function getItemOffer($id)
	{
		$id = (int) $id;
		foreach ($this->itemOffers as $offer) {
			if ($offer->id === $id) {
				return $offer;
			}
		}
		
		return NULL;
	}
	
	
	/**
	 * @param string
	 * @return bool
	 * @throws Nette\InvalidArgumentException
	 */
	public function canView($page)
	{
		if ($this->isAdmin()) {
			return TRUE;
		}
		if (!isset($this->viewPermissions->$page) || !is_bool($this->viewPermissions->$page)) {
			throw new Nette\InvalidArgumentException("invalid permissions page name ({$page})");
		}
		
		return $this->viewPermissions->$page;
	}
	
}