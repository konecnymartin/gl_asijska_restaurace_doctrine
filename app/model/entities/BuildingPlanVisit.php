<?php

namespace App\Model\Entities;

use Doctrine\ORM\Mapping as ORM,
	Nette,
	Kdyby;


/**
 * @ORM\Entity
 * @ORM\Table(name="building_plan_visit")
 */
class BuildingPlanVisit extends Kdyby\Doctrine\Entities\BaseEntity
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	
	/**
	 * @ORM\Column(name="`date`", type="datetime")
	 */
	protected $date;
	
	/**
	 * @ORM\OneToOne(targetEntity="User", fetch="EAGER")
	 */
	protected $user;
	
	/**
	 * @ORM\ManyToOne(targetEntity="BuildingPlan", fetch="EAGER")
	 */
	protected $plan;

}