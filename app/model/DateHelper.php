<?php

namespace App\Model;

class DateHelper
{
	
	private static $days = array(1 => "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota", "Neděle");
	
	private static $months = array(1 => "Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec");
	
	
	/**
	 * @param int
	 * @return string
	 */
	public static function getDayName($num)
	{
		return self::$days[$num];
	}
	
	
	/**
	 * @param int
	 * @return string
	 */
	public static function getMonthName($num)
	{
		return self::$months[$num];
	}
	
	
	/**
	 * @return string
	 */
	public static function getActualDayName()
	{
		$num = date("N");
		
		return self::getDayName($num);
	}
	
	
	/**
	 * @return string
	 */
	public static function getActualMonthName()
	{
		$num = date("n");
		
		return self::getMonthName($num);
	}
	
	
	/**
	 * @param \DateTime
	 * @return string
	 */
	public static function getDayNameOfDate(\DateTime $date)
	{
		// may not be the right way
		$format = $date->format("Y-m-d");
		$num = (int) date("N", strToTime($format));
		
		return self::getDayName($num);
	}
	
}